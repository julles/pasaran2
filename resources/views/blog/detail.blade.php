@extends('layouts.layout')

@section('content')
<link rel="stylesheet" href="{{ asset(null) }}css/user.style.css" type="text/css">
<link rel="stylesheet" href="{{ asset(null) }}css/style.css" type="text/css">
<style>
    .logo{
        width:250px;
    }
    
</style>

<div class="bg_content pagestyle  ">
	<div class="container search-bar horizontal collapse in">
		@include('layouts.search')
	</div>
</div>
			<div id="page-content">
                <section class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <header>
                                <h1 class="page-title">Blog</h1>
                            </header>
                            <article class="blog-post">
                                <header><a href="{{ url('blog/view/'.$list->slug) }}"><h2>{{ $list->title }}</h2></a></header>
                                <a href="{{ url('blog/view/'.$list->slug) }}"><img src="{{ asset('contents/'.$list->image) }}" alt=""></a>
                                <figure class="meta">
                                    <a href="#" class="link-icon"><!--i class="fa fa-user"></i>Admin-->&nbsp;</a>
                                    <a href="#" class="link-icon"><i class="fa fa-calendar"></i>{{ $list->created_at->format('d/m/Y') }}</a>
                                    <div class="tags">
                                    @if($list->categories()->count() > 0)
                                        @foreach($list->categories as $category)
                                            <a href="#" class="tag article">{{ $category->title }}</a>
                                        @endforeach
                                    @endif
                                    </div>
                                </figure>
                                <p>
                                    {!! $list->description !!}
                                </p>
                            </article><!-- /.blog-post -->
                            
                        </div>
                        <div class="col-md-3">
                            <aside id="sidebar">
                                
                                    <section>
                                        <header><h2>New Places</h2></header>
                                        @foreach($iklans as $iklan)
                                        <?php
                                        $image = $iklan->images()->first()->image;
                                        ?>
                                        <a href="{{ url('iklan/view-iklan/'.$iklan->slug) }}" class="item-horizontal small">
                                            <h3>{{ $iklan->judul }}</h3>
                                            <figure>{{ $iklan->gmap_address }}</figure>
                                            <div class="wrapper">
                                                <div class="image"><img src="{{ asset('contents/'.$image) }}" alt=""></div>
                                                <div class="info">
                                                    <div class="type">
                                                        <!--i><img src="icons/restaurants-bars/restaurants/restaurant.png" alt=""></i-->
                                                        <span>{{ $iklan->category->title }}</span>
                                                    </div>
                                                    <div class="rating" data-rating="4"></div>
                                                </div>
                                            </div>
                                        </a>
                                        @endforeach
                                    </section>
                                
                                <section>
                                    <a href="#"><img src="{{ asset(null) }}images/ad-banner-sidebar.png" alt=""></a>
                                </section>
                                <section style="margin-top:5%;">
                                    <header><h2>Categories</h2></header>
                                    <ul class="bullets">
                                    @foreach($categories as $cat)
                                        <li><a href="{{ url('blog/category/'.$cat->slug) }}" >{{ $cat->title  }}</a></li>
                                    @endforeach
                                    </ul>
                                </section>
                                <!--section>
                                    <header><h2>Events</h2></header>
                                    <div class="form-group">
                                        <select class="framed" name="events">
                                            <option value="">Select Your City</option>
                                            <option value="1">London</option>
                                            <option value="2">New York</option>
                                            <option value="3">Barcelona</option>
                                            <option value="4">Moscow</option>
                                            <option value="5">Tokyo</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group >
                                </section-->
                            </aside>
                            <!-- /#sidebar-->
                        </div>
                        <!-- /.col-md-3-->
                    </div>
                </section>
            </div>
</div>

@endsection
@section('script')
<script>
	var $ = jQuery.noConflict();
    if( $('body').hasClass('navigation-fixed') ){
        $('.off-canvas-navigation').css( 'top', - $('.header').height() );
        $('#page-canvas').css( 'margin-top',$('.header').height() );
    }
	$(document).ready(function($) {
		$('.off-canvas-navigation header').css( 'line-height', $('.header').height() + 'px' );
		"use strict";
		$(document).bind('keypress', 'M', function(){ 
			$('.header .toggle-navigation').trigger('click');
			return false;
		});
	});
</script>
@endsection
@extends('layouts.layout')

@section('content')
@include('layouts.menu')
<div class="bg_content">
	<section class="section_search" style="background:#fff url('images/material/pasaran-bg.png') no-repeat top center; background-size:cover">
		<div class="container">
			<h2>Cari kebutuhan apapun di manapun</h2>
			<div class="clear"></div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 marginauto" style="background:red; ">
				<div class="row">
					<form class="form-inline" role="form" type = 'get' action = '{{ url("search") }}'>
						<div class="rw">
							<div class="form-group col-lg-6 col-md-6 col-sm-5 col-xs-12 pd5"> 
								<input type="text" class="form-control" name="keyword" placeholder="Cari Kata Kunci : Kursi, Rumah, Mobil dll"/>
							</div>
							<div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-12 pd5"> 
								<div class="input-group">
									<input type="text" class="form-control" name="location" placeholder="Silahkan Pilih Lokasi ..."/> 
								</div>  
							</div>  
							<div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-12 pd5"> 
								<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div> 
						<div class="clear"></div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<div class="clear"></div>
	<section class="section_list_items section_style">
		<div class="container">
			<h3>TERBARU</h3>
			<div class="clear"></div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							@foreach($iklan->terbaru(4) as $baru)
								<div class="col-lg-3 col-md-3 col-sm-2 col-xs-12" >
									<div class="sl_item">
										
										<div class="sl_item_img" style="background:url('{{ asset("contents/".$baru->images()->first()->image) }}') no-repeat center center;background-size: cover;">
											<a class="icon_like" onclick = "like('{{ $baru->id }}')"><i class="fa fa-thumbs-up"></i></a>
											<a class="icon_view"><i class="fa fa-eye"></i></a>
											<div class="content_overlay"> </div>
											<div class="content_overlay_inner">
												<h5>Deskripsi</h5>
												<p>{!! $baru->deskripsi !!}</p>
											</div>
											<div class="time_label">{{ waktuLalu($baru->created_at) }}</div>
										</div>
										<div class="sl_item_detail" onclick = "window.location.href='{{ url('iklan/view-iklan/'.$baru->slug) }}'">
											<a href="{{ url('iklan/view-iklan/'.$baru->slug) }}"><h3>{{ $baru->judul }}</h3></a>
											<figure><i class="fa fa-map-marker"></i> {{ $baru->kota }}</figure>
											<div class="price">Rp. {{ money($baru->harga) }}</div>
											<div class="info">
												<div class="type">
													<i><!--img src="{{ asset(null) }}images/material/restaurant.png" alt=""--></i>
													<span>{{ $baru->category->title }}</span>
												</div>
											</div>
										</div>
										 
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
		</div>
	</section>
	<div class="clear"></div>
	<section class="section_populer section_style">
		<div class="container">
			<h3>POPULER</h3>
			 
			<div class="clear"></div>
				<div class="row"> 
					<div class="section_populer_slide">
						<div class="owl-populer">
						@foreach($iklan->populers(5) as $populer)
							<div class="item">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								
									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12"> 
											<img src="{{ asset("contents/".$populer->images()->first()->image) }}" class="img-responsive" style="width:100%"/> 
										</div>
										<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
											<div class="wrap_r">
												<div class="row_btn rb_head"> 
													<a href="{{ url('iklan/view-iklan/'.$populer->slug) }}"><h4>{{ $populer->judul }}</h4></a>
													<div class="fav pull-right">
														<div class="type">
															<a onclick = "like('{{ $populer->id }}')"><i class="fa fa-thumbs-up fa-lg"></i></a>
														</div>
													</div>
												</div>
												
												
												<figure>
													<i class="fa fa-map-marker"></i>
													<span>{{ $populer->gmap_address }}</span>
												</figure>
												<div class="clear"></div>
												<div class="row_btn"> 
													<div class="price">Rp. {{ money($populer->harga) }}</div> 
													<div class="type pull-right">
														<!--i><img src="{{ asset(null) }}images/material/restaurant.png"></i-->
														<span>{{ $populer->category->title }}</span>
													</div>
												</div>
												<div class="clear"></div>
												<p>
													{!! $populer->deskripsi !!}
												</p>
												<a href="{{ url('iklan/view-iklan/'.$populer->slug) }}" class="read-more">Read More</a>
											</div>
										</div>
									</div>
									
								</div>
							</div> 
						@endforeach
						</div>
					</div>  	 
				</div>
		</div>
	</section>
	<div class="clear"></div>
	<section class="section_whyus section_style">
		<div class="container">
			<h3>WHY US?</h3>
			<div class="clear"></div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<div class="feature-box">
									<i class="fa fa-search red"></i>
									<div class="description">
										<h3>Pencarian Lengkap</h3>
										<p>
                                           Praesent tempor a erat in iaculis. Phasellus vitae libero libero. Vestibulum ante
                                           ipsum primis in faucibus orci luctus et ultrices posuere cubilia
										</p>
									</div>
								</div> 
							</div> 
							<div class="col-md-4 col-sm-4">
								<div class="feature-box">
									<i class="fa fa-map green"></i>
									<div class="description">
										<h3>Pencarian Map</h3>
										<p>
                                           Pellentesque nisl quam, aliquet sed velit eu, varius condimentum nunc.
                                           Nunc vulputate turpis ut erat egestas, vitae rutrum sapien varius.
										</p>
									</div>
								</div> 
							</div> 
							<div class="col-md-4 col-sm-4">
								<div class="feature-box">
									<i class="fa fa-money blue"></i>
									<div class="description">
										<h3>Pemasangan Iklan Gratis</h3>
										<p>
                                           Maecenas quis ipsum lectus. Fusce molestie, metus ut consequat pulvinar,
                                           ipsum quam condimentum leo, sit amet auctor lacus nulla at felis.
										</p>
									</div>
								</div> 
							</div> 
                       </div>
					</div>
				</div> 
		</div>
	</section>
	<div class="clear"></div>
	<hr>
	<div class="clear"></div> 
	<section class="section_populerhariini section_style">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<h3>POPULER HARI INI</h3>
									<div class="clear"></div> 
									<div class="row">
										@foreach($iklan->populerToday() as $today)
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="sl_item">
													<div class="sl_item_img" style="background:url('{{ asset("contents/".$today->images()->first()->image ) }}') no-repeat center center;background-size: cover;">
														<div class="time_label">{{ waktuLalu($today->created_at) }}</div>
													</div>
													<div class="sl_item_detail">
														<a href="{{ url('iklan/view-iklan/'.$today->slug) }}"><h3>{{ $today->title }}</h3></a>
														<figure><i class="fa fa-map-marker"></i> {{ $today->kota }}</figure>
														<div class="price">Rp. {{ money($today->harga) }}</div>
														<div class="info">
															<div class="type">
																<!--i><img src="{{ asset(null) }}images/material/restaurant.png" alt=""></i-->
																<span>{{ $today->category->title }}</span>
															</div>
														</div>
													</div>
													 
												</div>
											</div>
										@endforeach
									</div> 
								</div>
							</div>
							
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 iklanpremium">
									<h3>IKLAN PREMIUM</h3>
									<div class="clear"></div>
									@foreach($iklan->iklanPremiums()->limit(5)->get() as $premium)
										<a href="{{ url('iklan/view-iklan/'.$premium->slug) }}" class="item-horizontal small">
                                        	<div class="icon icon-on pull-right" onclick = "like('{{ $premium->id }}')">
                                                <i class="fa fa-thumbs-up"></i>
                                            </div>
                                            <b><h3>{{ $premium->judul }}</h3></b>
                                            <figure><i class="fa fa-map-marker"></i> {{ $premium->provinsi }}</figure>
                                            <div class="wrapper">
                                                <div class="image"><img src="{{ asset('contents/'.$premium->images()->first()->image) }}" alt="" class="img-responsive"></div>
                                                <div class="info">
                                                	<div class="price"> 
                                                        <span>Rp. {{ money($premium->harga) }}</span>
                                                    </div>
                                                    <div class="type">
                                                        <!--i><img src="{{ asset(null) }}images/material/restaurant.png" alt=""></i-->
                                                        <span>{{ $premium->category->title }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<hr>
				</div>
			</div>
		</div>
	</section>
	
<div class="clear"></div>
</div>
@endsection
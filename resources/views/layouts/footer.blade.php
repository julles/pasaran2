	<div class="clear"></div> 
	<section id="footer" class="footer_section">
		<footer id="page-footer">
            <div class="inner">
                <div class="footer-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 col-sm-5 ">
                                <!--New Items-->
                                <section>
									<img src="{{ asset(null) }}images/material/logo/pasaran.png" style="width: 200px;margin-top: 10px;" alt="Pasaran Logo"> 
                                    <div class="wrapper">
                                      pasaran.com adalah situs pemasaran kebutuhan apapun dimanapun anda berada. Situs pemasaran No.1 di Indonesia ini memberikan banyak kemudahan terutama untuk pencarian barang, kendaraan, properti, pekerjaan, jasa, dan event disekitar anda.
                                    </div>
                                    <a href="{{ asset(null) }}about-us.html" class="read-more icon-readmore">Baca Selengkapnya</a>
                                </section>
                            </div>
                                <!--/.Social-media-->
                            <div class="col-md-2 col-sm-2">
                                <section>
                                        <h2><strong>Follow us</strong></h2>
                                        <a href="{{ asset(null) }}#" class="social-button"><i class="fa fa-twitter twitter"></i>Twitter</a>
                                        <a href="{{ asset(null) }}#" class="social-button"><i class="fa fa-facebook fb"></i>Facebook</a>
                                        <a href="{{ asset(null) }}#" class="social-button"><i class="fa fa-google gplus"></i>Google</a>
                                </section>
                            </div>
                                <!--/.End-social-media-->
                            <div class="col-md-2 col-sm-2">
                                <!--Recent Reviews-->
                                <section>
                                    <h2><strong>Navigasi</strong></h2>
                                    <a href="{{ asset('faq') }}" class="small"><h3>FAQ</h3></a>
                                    <a href="{{ url('blog') }}" class="small"><h3>Blog</h3></a>
                                    <a href="{{ url('tentang-kami') }}" class="small"><h3>Tentang Kami</h3></a>
                                    <a href="{{ url('syarat-dan-ketentuan') }}" class="small"><h3>Syarat dan Ketentuan</h3></a>
                                </section>
                                <!--end Recent Reviews-->
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <section>
                                    <h2><strong>Tentang Kami</strong></h2>
                                    <address>
                                        <div>Kencana Tower Lt 7</div>
                                        <div>Business Park Kebon Jeruk</div>
                                        <div>Jl. Meruya Ilir Raya No. 88, Kembangan</div>
                                        <div>Jakarta Barat, 11620</div>
                                        <figure>
                                            <div class="info">
                                                <i class="fa fa-phone"></i>
                                                <span>(021)30061568</span>
                                            </div>
                                        </figure>
                                    </address>
                                    <a href="{{ asset(null) }}contact.html" class="btn framed icon">Hubungi Kami<i class="fa fa-angle-right"></i></a>
                                </section>
                            </div>
                            <!--/.col-md-4-->
                        </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </div>
                <!--/.footer-top-->
                <div class="footer-bottom">
                    <div class="container">
                        <span class="left">Copyright © 2016 Pasaran.com. All Rights Reserved</span>
                            <span class="right">
                                <a href="{{ asset(null) }}#page-top" class="to-top roll"><i class="fa fa-angle-up"></i></a>
                            </span>
                    </div>
                </div>
                <!--/.footer-bottom-->
            </div>
        </footer>
	</section>	
	<div class="clear"></div>
  
	<a class="page-scroll" href="{{ asset(null) }}#page-top">
		<div id="scrolltotop" class="scrolltotop">
			<i class="fa fa-chevron-up"></i>
		</div>
	</a>
	<!-- jQuery -->
    <script src="{{ asset(null) }}js/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    
	
	<!-- carousel -->
	<script src="{{ asset(null) }}js/jquery.bxslider.min.js"></script>
	<script src="{{ asset(null) }}js/owl.carousel.js"></script>
	
    <script src="{{ asset(null) }}js/js_lib.js"></script>
    <script src="{{ asset(null) }}js/js_run.js"></script>
    <script src="{{ asset(null) }}js/TweenMax.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset(null) }}js/bootstrap.min.js"></script>

    <!-- Scrolling Nav JavaScript -->
    <script src="{{ asset(null) }}js/jquery.easing.min.js"></script>
    <script src="{{ asset(null) }}js/scrolling-nav.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;libraries=places"></script>    
    
	<script src="{{ asset(null) }}js/jquery.geocomplete.js"></script>
    <script src="{{ asset(null) }}js/dropzone.js"></script>
    <script src="{{ asset(null) }}backend/sweetalert/dist/sweetalert.min.js"></script>
	@yield('script')
    <script>
		$(function(){
			$("#geocomplete").geocomplete({
				map: ".map_canvas",
				details: "form",
				types: ["geocode", "establishment"],
			});

			$("#find").click(function(){
				$("#geocomplete").trigger("geocode");
			});
		});

        $(document).ready(function(){

           $("#dropzone_id").dropzone({ url: "file" });

       });

       function like(iklanId)
       {
            $.ajax({
                url : '{{ url("iklan/like") }}',
                data : {
                    iklanId : iklanId,
                },
                success : function(data){
                    $("#strong_like").text(data.count);
                }
            });
       }
       
       function initMap() {
                        var mapDiv = document.getElementById('mapna');
                        var map = new google.maps.Map(mapDiv, {
                          center: {lat: 44.540, lng: -78.546},
                          zoom: 8
                        });
                      }
       
	</script>
</body> 
</html>

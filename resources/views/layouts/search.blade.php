 
			<form class="main_search_bar" role="form" method="get" action="{{ url('search') }}">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="form-inline">
							<div class="rw">
								<div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 pd5"> 
									<input type="text" class="form-control bordershadow" name="keyword" placeholder="Cari Kata Kunci : Kursi, Rumah, Mobil dll"/>
								</div>
								<div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 pd5">   
									<div class="input-group add-on">
										<input type="text" class="form-control bordershadow" name="location" placeholder="Silahkan Pilih Lokasi ..."/> 
										<div class="input-group-btn">
											<button class="btn btn-default" type="submit">
												<i class="fa fa-map-marker geolocation" data-toggle="tooltip" data-placement="bottom" title="Lokasi Saya Sekarang"></i>
											</button>
										</div>
									</div>								
								</div>  
								<div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 pd5"> 
									<!--select class="selectpicker" data-live-search="true">
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select--> 
									{!! webarq::selectCategories(['class' => 'selectpicker','data-live-search'=>true]) !!}
								</div>
								<div class="form-group col-lg-1 col-md-1 col-sm-1 col-xs-12 pd5"> 
									<button type="submit" class="btn btn-default" style="width:100%;display:block;height:39px;"><i class="fa fa-search"></i></button>
								</div>
							</div> 
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</form> 
<div class="clear"></div>

<!DOCTYPE html>
<html lang="en"> 
<head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

   <title>Pasaran.com - Situs Pemasaran No.1 Di Indonesia</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset(null) }}css/bootstrap.min.css" rel="stylesheet">
    
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset(null) }}css/dropzone.css" type="text/css">
	
	
	<!-- Custom Fonts -->
	<link href="{{ asset(null) }}fonts/font-awesome.css" rel="stylesheet" type="text/css"> 
	
	<!-- Custom Animate --> 
	<link rel="stylesheet" type="text/css" href="{{ asset(null) }}css/set1.css" />
	
	
	<!-- carousel -->
	<link rel="stylesheet" type="text/css" href="{{ asset(null) }}css/jquery.bxslider.css" />
	<link rel="stylesheet" href="{{ asset(null) }}css/owl.carousel.css">
	<link rel="stylesheet" href="{{ asset(null) }}css/owl.theme.css">
	
    <!-- Custom CSS -->
    <link href="{{ asset(null) }}css/scrolling-nav.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link href="{{ asset(null) }}css/custom.css" rel="stylesheet">
	<link href="{{ asset(null) }}css/bootstrap-social.css" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="{{ asset(null) }}backend/sweetalert/dist/sweetalert.css">
	<link href="{{ asset(null) }}css/app.css" rel="stylesheet"> 
	
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="{{ asset(null) }}https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="{{ asset(null) }}https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
 
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<div class="header_top shadow">
	<div class="container">
		<div class="ht_left">
			<div class="ht_left_brand">
				<a href="{{ url('/') }}">
					<img src="{{ asset(null) }}images/material/logo/pasaran.png" class="logo" alt="Pasaran Logo">
					<span class="tagline"> Situs Pemasaran No.1 di Indonesia </span>
				</a>
			</div>
		</div>
		<div class="ht_right">
			<div class="ht_user_area">
				<ul>
					<li><a href="{{ url('search/favorits') }}"><i class="fa fa-thumbs-up fa-1x pull-left"></i><strong id = 'strong_like'>{{ count_likes() }}</strong></a></li>
					@if(!empty(me()->id))
						<li><a href="{{ url('account/logout') }}"><i class="fa fa-sign-out fa-1x pull-left"></i></i><strong>Keluar</strong></a></li>
						<li><a href="{{ url('account/me') }}"><i class="fa fa-user fa-1x pull-left"></i><strong>Akun Saya</strong></a></li>
						&nbsp;
						@if(!empty(request()->segment(1)))
							<a href = '{{ url("account/pasang-iklan") }}' class="btn btn-success" style="font-size:20px;background-color:#03a9f4;color:white;">
	    	                    <!--i class="fa fa-plus fa-2x pull-left"></i-->Pasang Iklan
		                    </a>
		                @endif
					@else	
						<li><a href="{{ url('account/my-account') }}"><i class="fa fa-user fa-1x pull-left"></i><strong>Login</strong></a></li>
					@endif
					
                        
				</ul>
			</div>
		</div>
		
	</div>
</div>
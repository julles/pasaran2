@extends('layouts.layout')

@section('content')
<style>
	.fieldsetmaps{
		background:yellow;
		margin-left:50px;
		display:none;
	}
	.fieldsetmaps input {
		height:10px;
		width:100px;
	}
    .map_canvas{
		display:block;
		width:100%;
		height:300px;
	} 
</style>
 
<div class="bg_content pagestyle pagecontent">
	<div class="container search-bar horizontal collapse in">
		@include('layouts.search')
	</div> 
	<div class="container signinsection">
		<div class="row">
			<div class="col-md-3 col-sm-4 col-md-offset-3 col-sm-offset-1">
				<header>
					<h1 class="page-title">Masuk ke akun Pasaran</h1>
				</header>
				<hr>
				{!! Form::open(['id' => 'form-sign-in-account']) !!}
					<div class="form-group">
						<label for="form-sign-in-email">Email:</label>
						{!! Form::text('email',null,['class' => 'form-control']) !!}
					</div><!-- /.form-group -->
					<div class="form-group">
						<label for="form-sign-in-password">Kata Sandi:</label>
						{!! Form::password('password',null,['class' => 'form-control']) !!}
					</div><!-- /.form-group -->
					<div class="form-group">
						<input type="checkbox" class="lupa-pass pull-left" id="form-ingat-saya" name="form-ingat-saya" checked>
						<label for="form-ingat-saya">Biarkan saya tetap masuk</label>
					</div><!-- /.form-group -->
					<div class="form-group clearfix">
						<span class="pull-left"><a href="#">Lupa Password ?</a></span>
						<button type="submit" class="btn pull-right btn-default" id="account-submit">Masuk</button>
					</div><!-- /.form-group -->
				{!! Form::close() !!}
			</div>
			<div class="col-offset-1">
				<div class="border"><img src="{{ asset(null) }}images/material/border.png"></div>
			</div>
			<div class="col-md-3 col-sm-4">
				<header>
					<h5 class="page-title pt_register">Belum Punya Akun ? <a href="{{ url('account/daftar') }}">Daftar Sekarang!</a></h5>
				</header>
				<div class="clear"></div>
				<div class="social-login">
					<a class="btn btn-block btn-social btn-twitter" href = "{{ url('social/login/twitter') }}">
						<span class="fa fa-twitter"></span>
						Sign in with Twitter
					</a>
					<a class="btn btn-block btn-social btn-facebook" href = "{{ url('social/login/facebook') }}">
						<span class="fa fa-facebook"></span>
						Sign in with Facebook
					</a>
					<a class="btn btn-block btn-social btn-google" href = "{{ url('social/login/google') }}">
						<span class="fa fa-google"></span>
						Sign in with Google
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{{ asset(null) }}http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script src="{{ asset(null) }}http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
@endsection
@section('script')
	
	@if($errors->any())

		{!! flashValidation($errors->all()) !!}	

	@endif

	@if(Session::has('success'))

		{!! flash('Sukses',Session::get('success'),'success') !!}

	@endif

	@if(Session::has('failed'))

		{!! flash('Warning',Session::get('failed'),'warning') !!}

	@endif

@endsection
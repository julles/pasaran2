<div class="col-md-3 col-sm-3">
                            <aside id="sidebar">
                                <ul class="navigation-sidebar list-unstyled">
                                    <li class="active">
                                        <a href="{{ url('account/my-inbox') }}">
                                            <i class="fa fa-envelope"></i>
                                            <span>Semua Pesan</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('account/my-inbox-all') }}">
                                            <i class="fa fa-inbox"></i>
                                            <span>Kotak masuk</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('account/sent') }}">
                                            <i class="fa fa-paper-plane"></i>
                                            <span>Kotak Keluar</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa fa-archive"></i>
                                            <span>Arsip</span>
                                        </a>
                                    </li>
                                </ul>
                            </aside>
                        </div>
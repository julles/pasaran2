@extends('layouts.layout')
@section('content')
<link rel="stylesheet" href="{{ asset(null) }}css/user.style.css" type="text/css">
<link rel="stylesheet" href="{{ asset(null) }}css/style.css" type="text/css">
<style>
	.logo{
		width:250px;
	}
</style>
<div class="bg_content pagestyle pagecontent">
	<div class="container search-bar horizontal collapse in">
		@include('layouts.search')
	</div>

        <div id="page-canvas"> 
            <div id="page-content">
                <section class="container">
                    <header>
                        <ul class="nav nav-pills">
                            <li class="active"><a href="{{ url('account/me') }}"><h1 class="page-title">{{ auth()->user()->name }}</h1></a></li>
                            <li><a href="{{ url('account/my-iklan') }}"><h1 class="page-title">Iklan Saya</h1></a></li>
                            <li><a href="{{ url('account/my-inbox') }}"><h1 class="page-title">Inbox</h1></a></li>
                        </ul>
                    </header>
					
                    @yield('profile');

					
                </section>
            </div>
        </div>
	</div>

@endsection
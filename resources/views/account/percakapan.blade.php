@extends('account.me')
@section('profile')
                    <div class="row">
                        @include('account.sidebar_percakapan')
                       <div class="col-md-9 col-sm-9">
                            <section id="massage">
                            @foreach($chats as $chat)
                            <?php
                            if($chat->from == auth()->user()->id)
                            {
                                $pull = 'right';
                            }else{
                                $pull = 'left';
                            }
                            ?>
                                <div class="massage daftar admin-view pull-{{ $pull }}">
                                    <div class="wrapper">
                                       <h3>Dijual Toyota Innova G Manual 2008 silver metalik sehat terawat</h3>
                                        <figure><a href="#">{{ $chat->nama }}</a></figure><br>
                                        <div class="content">
                                            <p>{{ $chat->pesan }}</p>
                                        </div><br>
                                    </div>
                                    <div class="time-message">
                                        <i class="fa fa-clock-o"> {{ waktuLalu($chat->created_at) }} , {{ carbon()->parse($chat->created_at)->format("H:i:s") }}</i>
                                    </div>
                                </div>
                            @endforeach
                                <!-- /.Iklan Info-->

                                    <div class="wrapper">
                                    {!! Form::open() !!}
                                        <textarea placeholder="Isi Pesan..." class="kirim-pesan" name = 'pesan'></textarea>
                                        <button id="submit" class="btn btn-default pull-right" type="submit">Kirim</button>
                                    {!! Form::close() !!}
                                    </div>
                            </section>
                        </div>
                    </div>
@endsection

@section('script')
    
    @if($errors->any())

        {!! flashValidation($errors->all()) !!} 

    @endif

    @if(Session::has('success'))

        {!! flash('Sukses',Session::get('success'),'success') !!}

    @endif

@endsection
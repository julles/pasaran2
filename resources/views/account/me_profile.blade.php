@extends('account.me')
@section('profile')
<div class="row">
    <div class="col-md-9">
        <form id="form-profile" role="form" method="post" action="?" enctype="multipart/form-data">
        {!! Form::model($model,['id'=>'form-profile','files'=>true]) !!}
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <section>
                        <h3><i class="fa fa-image"></i>Foto Profile</h3>
                        <div id="profile-picture" class="profile-picture dropzone">
                            <input name="image" type="file">
                            <!--div class="dz-default dz-message"><span>Klik atau tarik gambar ke sini</span></div-->
                            <img style="width: 100%;" src="{{ asset('contents/'.$model->image) }}" alt="">
                        </div>
                        <input name="image" type="file">
                    </section>
                </div>
                <div class="col-md-9 col-sm-9">
                    <section>
                        <h3><i class="fa fa-user"></i>Identitas Diri</h3>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="name">Nama Lengkap</label>
                                    {!! Form::text('name' , null ,['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    {!! Form::text('email' , null ,['class' => 'form-control','readonly'=>true]) !!}
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="phone">No Handphone</label>
                                    {!! Form::text('no_handphone' , null ,['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="pin-bb">Pin BB</label>
                                    {!! Form::text('pin_bb' , null ,['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <input type="checkbox" {{ $model->whatsapp == 'yes' ? 'checked' : '' }} class="whatsapp pull-left" id="whatsapp" name="whatsapp">
                                    <label for="whatsapp">Saya bisa dihubungi via whatsapp <img src="css/images/whatsapp.png" alt="whatsapp"></label>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                     <button type="submit" class="btn btn-large btn-default pull-right" id="submit">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
    <div class="col-md-3 col-sm-9">
        <h3><i class="fa fa-asterisk"></i>Ubah Password</h3>
        {!! Form::open(['url' => 'account/change-password' ,'class' => 'framed']) !!}
            <div class="form-group">
                <label for="new-password">Password Baru</label>
                {!! Form::password('password',['class' => 'form-control','placeholder' => 'Minimal 8 karakter']) !!}
            </div>
            <div class="form-group">
                <label for="confirm-new-password">Ulangi Password Baru</label>
                {!! Form::password('verify_password',['class' => 'form-control','placeholder' => 'Minimal 8 karakter']) !!}
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default">Ubah Password</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('script')
    
    @if($errors->any())

        {!! flashValidation($errors->all()) !!} 

    @endif

    @if(Session::has('success'))

        {!! flash('Sukses',Session::get('success'),'success') !!}

    @endif

@endsection
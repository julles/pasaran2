@extends('account.me')
@section('profile')
                    <div class="row">
                        @include('account.sidebar_percakapan')
                        <div class="col-md-9 col-sm-9">
                            <section id="items">
                            @foreach($messages as $message)
                                <?php
                                
                                if($message->from == auth()->user()->id)
                                { 
                                    $label = 'Terkirim ';
                                    $nameEmail = $message->nama_iklan.'< '.$message->email_iklan.'>'; 
                                }else{
                                 $label = "";
                                 $nameEmail = "";
                                }
                                
                                if($message->to == auth()->user()->id)
                                { 
                                    $label = 'Pesan Dari ';
                                    $nameEmail = $message->nama.'< '.$message->email.'>'; 
                                }

                                if($message->user_id == null)
                                {
                                     $label = 'Pesan Dari ';
                                    $nameEmail = $message->nama.' < '.$message->email.'>'; 
                                }

                                ?>

                                <div class="item list admin-view">
                                    <div class="wrapper">
                                        <a href="{{ $ifUser($message->email,$message->user_id,'url') }}"><h3>{{ substr(strip_tags($message->pesan),0,200) }} {{ $ifUser($message->email,$message->user_id,'user') }} </h3></a>
                                        <figure>{{ $label }}<a href="{{ $ifUser($message->email,$message->user_id,'url') }}">{{ $nameEmail }}</a></figure></br>
                                        <div class="content">
                                            <p>{!! strip_tags($message->pesan) !!}</p>
                                        </div></br>
                                    </div>
                                    <div class="ribbon in-queue">
                                        <i class="fa fa-archive" data-toggle="tooltip" title="Pesan Sudah Diarsipkan"></i>
                                    </div>
                                </div>
                            @endforeach
                            {!! $messages->render() !!}
                            </section>
                        </div>
                    </div>
@endsection

@section('script')
    
    @if($errors->any())

        {!! flashValidation($errors->all()) !!} 

    @endif

    @if(Session::has('success'))

        {!! flash('Sukses',Session::get('success'),'success') !!}

    @endif

@endsection
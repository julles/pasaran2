@extends('account.me')
@section('profile')
<?php
    $status = function($status){
        switch ($status) {
            case 'y':
                return 'fa fa-check';
            break;
            case 'p':
             return 'fa fa-clock-o';
            break; 
            case 'n':
             return 'fa fa-eye-slash';
            break; 
            case 'premium':
             return 'fa fa-money';
            break; 
            default:
                return 'fa fa-clock-o';
            break;
        }
    };
    $ribbon = function($ribbon){
        switch ($ribbon) {
            case 'y':
                return 'ribbon approved';
            break;
            case 'p':
             return 'ribbon in-queue';
            break; 
            case 'n':
             return 'ribbon in-non-aktif';
            break; 
            default:
                return 'ribbon in-queue';
            break;
        }
    };
?>
<div class="row">
    <div class="col-md-3 col-sm-3">
        <aside id="sidebar">
            <ul class="navigation-sidebar list-unstyled">
                <li class="{{ empty(request()->segment(3)) ? 'active' : '' }}">
                    <a href="{{ url('account/my-iklan') }}">
                        <i class="fa fa-folder"></i>
                        <span>Semua Iklan Saya</span>
                    </a>
                </li>
                <li class="{{ (request()->segment(3) == 'y') ? 'active' : '' }}">
                    <a href="{{ url('account/my-iklan/y') }}">
                        <i class="fa fa-check"></i>
                        <span>Aktif</span>
                    </a>
                </li>
                <li class="{{ (request()->segment(3) == 'p') ? 'active' : '' }}">
                    <a href="{{ url('account/my-iklan/p') }}">
                        <i class="fa fa-clock-o"></i>
                        <span>Menunggu verifikasi</span>
                    </a>
                </li>
                <li class="{{ (request()->segment(3) == 'n') ? 'active' : '' }}">
                    <a href="{{ url('account/my-iklan/n') }}">
                        
                        <i class="fa fa-eye"></i>
                        
                        <span>Tidak Aktif</span>
                    </a>
                </li>
            </ul>
        </aside>
    </div>
    <div class="col-md-9 col-sm-9">
        <section id="items">
            @foreach($iklans as $row)
                <div class="item list admin-view">
                    <div class="image">
                        <a href="{{url('iklan/view-iklan/'.$row->slug)}}">
                            <div class="overlay">
                                <div class="inner">
                                    <div class="content">
                                        <h4>Deskripsi</h4>
                                        <p>{{ $row->deskripsi }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item-specific">
                                <span title="timestamp">{{ waktuLalu($row->created_at) }}</span>
                            </div>
                            <img src="{{ asset('contents/'.@$row->images()->first()->image) }}" alt="">
                        </a>
                    </div>
                    <div class="wrapper">
                        <a href="{{url('iklan/view-iklan/'.$row->slug)}}"><h3>{{ $row->judul }}</h3></a>
                        <figure>{{ $row->gmap_address }}</figure>
                        <div class="info">
                            <div class="type">
                                <i class="fa fa-eye"></i>
                                <span>Dilihat {{ $row->views }} kali</span>
                            </div>
                            <div class="type">
                                <a href="#"><!--i><img src="{{ asset(null) }}assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i-->
                                <span> { {{ $row->category->title }} }</span></a>
                            </div> 
                        </div>
                    </div>
                    <div class="description">
                        <ul class="list-unstyled actions">
                            <li><a href="{{ url('account/edit-iklan/'.$row->id) }}" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a></li>
                            @if($row->status != 'p')
                            <li>
                                <a href="{{ url('account/status/'.$row->id) }}" data-toggle="tooltip" title="{{ ($row->status == 'n') ? 'Aktifkan' : 'Non-Aktifkan' }}">
                                    <i class="fa fa-eye{{ ($row->status == 'n') ? '-slash' : '' }}"></i>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                    <div class="{{ $ribbon($row->status) }}">
                        <i class="{{ $status($row->status) }}"></i>
                    </div>
                </div>
                @endforeach
            <!-- /.item-->
        </section>
    </div>
</div>
@endsection

@section('script')
    
    @if($errors->any())

        {!! flashValidation($errors->all()) !!} 

    @endif

    @if(Session::has('success'))

        {!! flash('Sukses',Session::get('success'),'success') !!}

    @endif

@endsection
@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user"> {{ webarq::titleActionForm() }}</h3>
    </div>
        <div id="content_body">
            
            <div class = 'row'>

                <div class = 'col-md-9'>

                    @include('backend.common.errors')

                     {!! Form::model($model,['files' => 'true']) !!} 

                      <div class="form-group">
                        <label>Question</label>
                        {!! Form::textarea('question' , null ,['class' => 'form-control']) !!}
                      </div>
                       
                      <div class="form-group">
                        <label>Answer</label>
                        {!! Form::textarea('answer' , null ,['class' => 'form-control']) !!}
                      </div>

                      <div class="form-group">
                        <label>Order</label>
                        {!! Form::text('order' , null ,['class' => 'form-control']) !!}
                      </div>

                      <div class="form-group">
                        <label>Status</label>
                        {!! Form::select('status' , ['y' => 'Active','n'=>'Is Active'] , null ,['class' => 'form-control']) !!}
                      </div>

                      <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    
                    {!! Form::close() !!}

                </div>

            </div>

        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function(){
    $('#my-select').multiSelect()
  });
  window.onload = function()
  {
      CKEDITOR.replace( 'description',{
      filebrowserBrowseUrl: '{{ urlBackend("image/lib")}}'});
  }
</script>
@endsection
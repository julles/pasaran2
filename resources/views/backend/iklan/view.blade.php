@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">Owner Detail</h3>
    </div>
        <div id="content_body">
            
            <div class = 'row'>

                <div class = 'col-md-5'>
                      <div class="form-group">
                        <label>Name</label>
                        {!! Form::text('name' , $model->user->name ,['class' => 'form-control','readonly' => true]) !!}
                      </div>

                      <div class="form-group">
                        <label>No Handphone</label>
                        {!! Form::text('no_handphone' , $model->user->no_handphone ,['class' => 'form-control','readonly' => true]) !!}
                      </div>

                      <div class="form-group">
                        <label>Pin BB</label>
                        {!! Form::text('pin_bb' , $model->user->pin_bb ,['class' => 'form-control','readonly' => true]) !!}
                      </div>

                      <div class="form-group">
                        <label>Email</label>
                        {!! Form::text('email' , $model->user->email ,['class' => 'form-control','readonly' => true]) !!}
                      </div>
                </div>

            </div>

        </div>
    </div>

    <div id="app_content">
    <div id="content_header">
        <h3 class="user">Iklan Detail</h3>
    </div>
        <div id="content_body">
            
            <div class = 'row'>
              <div class = 'col-md-9'>
              {!! Form::model($model) !!}
                      <div class="form-group">
                        <label>Judul</label>
                        {!! Form::text('name' , $model->judul ,['class' => 'form-control','readonly' => true]) !!}
                      </div>

                      <div class="form-group">
                        <label>Category</label>
                        {!! Form::text('category_id' , $model->category->title ,['class' => 'form-control','readonly' => true]) !!}
                      </div>

                      <div class="form-group">
                        <label>Deskripsi</label>
                        {!! Form::textarea('pin_bb' , $model->deskripsi ,['class' => 'form-control','readonly' => true]) !!}
                      </div>

                      <div class="form-group">
                        <label>Provonsi</label>
                        {!! Form::text('provinsi' , $model->provinsi ,['class' => 'form-control','readonly' => true]) !!}
                      </div>

                      <div class="form-group">
                        <label>Kota</label>
                        {!! Form::text('provinsi' , $model->provinsi ,['class' => 'form-control','readonly' => true]) !!}
                      </div>

                      <div class="form-group">
                        <label>Jalan</label>
                        {!! Form::text('gmap_address' , $model->gmap_address ,['class' => 'form-control','readonly' => true]) !!}
                      </div>

                      <div class="form-group">
                        <label>Harga</label>
                        {!! Form::text('harga' , $model->harga ,['class' => 'form-control','readonly' => true]) !!}
                      </div>

                      <div class="form-group">
                        <label>Status</label>
                        {!! Form::select('status' , ['p' => 'pending','y'=>'Active','n'=>'In Active','premium'=>'Premium'] , $model->status ,['class' => 'form-control']) !!}
                      </div>
                      
                      <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                </div>
              {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection
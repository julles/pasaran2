@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user"> {{ webarq::titleActionForm() }}</h3>
    </div>
        <div id="content_body">
            
            <div class = 'row'>

                <div class = 'col-md-6'>

                    @include('backend.common.errors')
                    @include('backend.common.flashes')
                     
                     {!! Form::model($model) !!} 

                      <div class="form-group">
                        <label>Parent</label>
                        {!! Form::select('category_id' ,$parents, null ,['class' => 'form-control']) !!}
                      </div>

                      <div class="form-group">
                        <label>Title</label>
                        {!! Form::text('title' , null ,['class' => 'form-control']) !!}
                      </div>

                      <div class="form-group">
                        <label>Icon</label>
                        {!! Form::text('icon' , null ,['class' => 'form-control','placeholder' => 'fa fa-example']) !!}
                      </div>
                      
                      <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    
                    {!! Form::close() !!}

                </div>

            </div>

        </div>
    </div>
@endsection
@extends('backend.layouts.layout')
@section('content')
<?php
    function childs($parent,$no)
    {
        $model = injectModel('Category');
        
        $no = $no + 1;

        $strip = "";

        for($i=0;$i<=$no;$i++)
        {
            $strip .= " - ";
        }

        $childs = "";

        foreach($model->whereCategoryId($parent->id)->get() as $row)
        {
            $childs .= "
            <tr>
                <td>".$strip.$row->title."</td>
                <td>".\webarq::buttons($row->id)."</td>
            </tr>
            ";

            $childs .= childs($row,$no);
        }

        return $childs;
    }


?>


<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>
           <div class = 'col-md-12'>

                    {!! webarq::buttonCreate() !!}
                
                
                <p>&nbsp;</p>
                <p>&nbsp;</p>

                <table class = 'table' id = 'table'>
                    <thead>
                        <tr>
                            <th width = '80%'>Title</th>
                            <th width = '20%'>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $no = 0; ?>
                        @foreach($model as $parent)

                            <tr>
                                <td width = ''>{{ $parent->title }}</td>
                                <td width = ''>{!! webarq::buttons($parent->id); !!}</td>
                            </tr>
                            {!! childs($parent,$no) !!}
                        @endforeach

                    </tbody>
                </table>

            </div>

        </div>

        


    </div>
</div>
@endsection

@section('script')
    
    <script type="text/javascript">
        
        $(document).ready(function(){
            $('#table').DataTable({
                ordering:false,
            });
        });

    </script>

@endsection
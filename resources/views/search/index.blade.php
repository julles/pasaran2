@extends('layouts.layout')

@section('content')

<div class="bg_content pagestyle pagecontent">
	<div class="container search-bar horizontal collapse in">
		@include('layouts.search')
	</div> 
	<div class="container"> 
			@include('search.filter') 
			<div class="clear"></div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				 
				<div class="row">
				@if($model->count() >  0)
					@foreach($model as $row)
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" >
							<div class="sl_item"> 
								<div class="sl_item_img" style="background:url('{{ asset("contents/".$row->images()->first()->image) }}') no-repeat center center;background-size: cover;">
									<a class="icon_like" onclick = "like('{{ $row->id }}')"><i class="fa fa-thumbs-up"></i></a>
									<a class="icon_view"><i class="fa fa-eye"></i>
										<span class="count_view">Dilihat {{ $row->views }} kali</span>
									</a>
									<!-- overlay -->
									<div class="content_overlay_bg"> </div>
									<div class="content_overlay"> </div>
									<div class="content_overlay_inner">
										<h5>Deskripsi</h5>
										<p>{!! substr($row->deskripsi,0,20) !!}. . .</p>
									</div>
									<!-- end overlay -->
								<div class="time_label">{{ waktuLalu($row->created_at) }}</div>
								</div>
								<div class="sl_item_detail" onclick = "window.location.href='{{ url('iklan/view-iklan/'.$row->slug)  }}'">
									<a href="{{ url('iklan/view-iklan/'.$row->slug) }}"><h3>{{ $row->judul }}</h3></a>
									<figure><i class="fa fa-map-marker"></i> {{ $row->kota }}</figure>
									<div class="price">Rp. {{ money($row->harga) }}</div>
									<div class="info">
										<div class="type">
											<!--i><img src="{{ asset(null) }}images/material/restaurant.png" alt=""></i-->
											<span>{{ $row->category->title }}</span>
										</div>
									</div>
								</div> 
							</div>
						</div>
					@endforeach

				@else
					<div style="margin-bottom: 20%;"><h2>WHOOPS ! Data yang anda cari tidak ditemukan</h2></div>
				@endif
				</div>
				@if($pagination->count() > 0)
					
					{!! $pagination->render() !!}

				@endif
			</div>
		</div>
	</div>
</div>

@endsection
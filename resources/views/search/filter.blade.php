<?php
	
	$get = function($par){
		return request()->get($par);
	};

	$url = function($view) use ($get){
			
		$before = '?keyword='.$get('keyword').'&location='.$get('location').'&category_id='.$get('category_id').'&page='.$get('page');

		return request()->url().$before.'&view='.$view;
	};
?>
<figure class="filter "> 
			<div class="pull-left">
				<ol class="breadcrumb">
					<li><a href="index-directory.html"><i class="fa fa-home"></i></a></li>
					<li><a href="#">Pencarian</a></li>
					<li class="active">Grid</li>
				</ol> 
			</div>
			<div class="buttons pull-right">
				<a href="{{ $url('grid') }}" class="btn icon {{ ($get('view') == 'grid') ? 'active' : '' }}"><i class="fa fa-th"></i>Grid</a>
				<a href="{{ $url('list') }}" class="btn icon {{ ($get('view') == 'list') ? 'active' : '' }}"><i class="fa fa-th-list"></i>List</a>
				<a href="{{ $url('map') }}" class="btn icon {{ ($get('map') == 'grid') ? 'active' : '' }}"><i class="fa fa-map-marker"></i>Map</a>
			</div>
			<!--div class="pull-right">
				<aside class="sorting">
					<span>Sorting</span>
					<div class="form-group">
						<select class="selectpicker" data-live-search="true">
							<option>Mustard</option>
							<option>Ketchup</option>
							<option>Relish</option>
						</select> 
					</div> 
				</aside>
			</div--> 
</figure>
@extends('layouts.layout')

@section('content')
<link rel="stylesheet" href="{{ asset(null) }}css/user.style.css" type="text/css">
<link rel="stylesheet" href="{{ asset(null) }}css/style.css" type="text/css">
<style>
	.logo{
		width:250px;
	}
	
</style>
<div class="bg_content pagestyle pagecontent">
	<div class="container search-bar horizontal collapse in">
		@include('layouts.search')
	</div>

        <div id="page-canvas"> 
            <div id="page-content page-item-detail">
				<div id="map-detail"></div>
                <section class="container">
                    <div class="row">
                        <!--Item Detail Content-->
                        <div class="col-md-9">
                            <section class="block" id="main-content">
                                <header class="page-title">
                                    <div class="title">
                                        <h1>{{ $model->judul }}</h1>
                                        <figure>{{ $model->gmap_address }}</figure>
                                    </div>
                                    <div class="fav">
                                        <div class="type">
                                            <a href="#" data-toogle="tooltip" title="Suka"><i class="fa fa-thumbs-up fa-lg"></i></a>
                                        </div>
                                    </div>
                                </header>
                                <div class="row">
                                    <!--Detail Sidebar-->
                                    <aside class="col-md-4 col-sm-4" id="detail-sidebar">
                                        <section>
                                            <header><h3><div class="btn_price">Rp. {{ money($model->harga) }}</div></h3></header>
                                        </section>
                                        <!--Contact-->
                                        <section class="box-white">
                                            <header><h3>{{ $model->user->name }}</h3></header>
                                            <address>
                                                <div>Member sejak {{ Carbon\Carbon::parse($model->user->creatd_at)->format("F Y") }} </div>
                                                <div>Terakhir login <b>{{ $lastLogin }}</b></div>
                                                <figure>
                                                    <div class="info">
                                                        <img src="{{ asset(null) }}images/material/whatsapp.png" alt="whatsapp">
                                                        <span><b> {{ $model->user->no_handphone }}</b></span>
                                                    </div>
                                                    <div class="info">
                                                        <img src="{{ asset(null) }}images/material/bbm.png" alt="bbm">
                                                        <span><b> {{ $model->user->pin_bb }}</b></span>
                                                    </div>
                                                </figure>
                                            </address>
                                        </section>
                                        <!--Pesan-->
                                        @if(auth()->check())

                                            @if(auth()->user()->id != $model->user->id)
                                                {!! Form::open(['id'=>'item-detail-form','url' => 'iklan/kirim-pesan/'.$model->id]) !!}
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <label for="item-detail-message">Pesan</label>
                                                        {!! Form::textarea('pesan',null,['class' => 'form-control framed']) !!}
                                                   
                                                    </div>
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <button type="submit" class="btn framed icon">Kirim<i class="fa fa-angle-right"></i></button>
                                                    </div>
                                                    <!-- /.form-group -->
                                                {!! Form::close() !!}
                                            @endif
                                        @else
                                        <section class="box-white">
                                            <header><h3>Kontak Ke Penjual</h3></header>
                                            <figure>
                                                
                                                {!! Form::open(['id'=>'item-detail-form','url' => 'iklan/kirim-pesan/'.$model->id]) !!}
                                                    <div class="form-group">
                                                        <label for="item-detail-name">Nama</label>
                                                       {!! Form::text('nama',null,['class' => 'form-control framed']) !!}
                                                    </div>
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <label for="item-detail-email">Email</label>
                                                    	{!! Form::text('email',null,['class' => 'form-control framed']) !!}
                                                   
                                                    </div>
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <label for="item-detail-message">Pesan</label>
                                                    	{!! Form::textarea('pesan',null,['class' => 'form-control framed']) !!}
                                                   
                                                    </div>
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <button type="submit" class="btn framed icon">Kirim<i class="fa fa-angle-right"></i></button>
                                                    </div>
                                                    <!-- /.form-group -->
                                                {!! Form::close() !!}
                                            </figure>
                                        </section>
                                        @endif
                                        <!--end Pesan-->
                                    </aside>
                                    <!--end Detail Sidebar-->
                                    <!--Content-->
                                    <div class="col-md-8 col-sm-8">
                                        <section>
                                            <article class="item-gallery">
                                                <div class="owl-carousel item-slider">
                                                <?php $no = 0; ?>
                                                @foreach($model->images as $a)
                                                <?php $no++; ?>
														<div class="slide"><img src="{{ asset('contents/'.$a->image) }}" data-hash="{{ $no }}" alt=""></div>
                                                @endforeach
                                                </div>
                                                <!-- /.item-slider -->
                                                <div class="thumbnails">
                                                    <span class="expand-content btn framed icon" data-expand="#gallery-thumbnails" ><i class="no-margin fa fa-plus"></i></span>
                                                    <div class="expandable-content height collapsed show-70" id="gallery-thumbnails">
                                                        <div class="content">
                                                        <?php $no = 0; ?>
                                                        @foreach($model->images as $image)
                                                         <?php $no++; ?>
                                                            <a href="#{{ $no }}" id="thumbnail-{{ $no }}" class="{{ $no == 1 ? 'active' : '' }}"><img src="{{ asset('contents/'.$image->image) }}" alt=""></a>
                                                       	@endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- /.item-gallery -->
                                            <article class="block">
                                                <header><i class="fa fa-eye pull-right"> {{ $model->views }} kali </i><h2>Deskripsi</h2></header>
                                                <p>
                                                    {{ $model->deskripsi }}
                                                </p>
                                            </article>
                                            <!-- /.block -->
                                        </section>
                                        <section>
                                            <!--h3><a href="#"><i class="fa fa-envelope"> Kirim Ke Teman </i></a>
                                            <a href="#" class="pull-right"><i class="fa fa-exclamation-circle"> Laporkan Penjual</i></a></h3-->
                                        </section>
                                    </div>
                                    <!-- /.col-md-8-->
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /#main-content-->
                        </div>
                        <!-- /.col-md-8-->
                        <!--Sidebar-->
                        <div class="col-md-3">
                            <aside id="sidebar">
                                 <!--Sharing-->
                                <section class="box-white-left clearfix">
                                        <ul class="social-nav sharing pull-right">
                                            <li>Bagikan</li>
                                            <li><a href="https://twitter.com/intent/tweet?url={{ request()->url() }}" class="twitter" data-toggle="tooltip" title="Bagikan di Twitter"><i class="fa fa-twitter"></i></a></li>
                                            <li><a target="_blank" href="http://facebook.com/sharer/sharer.php?u={{ request()->url() }}" class="facebook" data-toggle="tooltip" title="Bagikan di Facebook"> <i class="fa fa-facebook"></i></a></li>
                                            <li><a target="_blank" href="https://plus.google.com/share?url={{ request()->url() }}" class="google-plus" data-toggle="tooltip" title="Bagikan di Google +"><i class="fa fa-google-plus"></i></a></li>
                                        </ul>
                                </section>
                                        <!--end Sharing-->
                                <section>
                                    <h2>Iklan Terkait</h2>
                                    @foreach($iklanTerkait as $terkait)
                                    <a href="{{ url('iklan/view-iklan/'.$terkait->slug) }}" class="item-horizontal small">
                                        <h3>{{ $terkait->judul }}</h3>
                                        <figure>{{ $terkait->gmap_address }}</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="{{ asset('contents/'.$terkait->images()->first()->image) }}" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <!--i><img src="{{ asset(null) }}images/material/restaurant.png" alt=""></i-->
                                                    <span>{{ $terkait->category->title }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    @endforeach
                                    
                                    <!--/.item-horizontal small-->
                                </section>
                            </aside>
                            <!-- /#sidebar-->
                        </div>
                        <!-- /.col-md-3-->
                        <!--end Sidebar-->
                    </div><!-- /.row-->
                </section>
            </div>
        </div>
	</div>


@endsection
@section('script')

<script>
	"use strict";
	var $ = jQuery.noConflict();

	var mapStyles = [
		{
			"featureType": "all",
			"elementType": "all",
			"stylers": [
				{
					"saturation": -100
				},
				{
					"gamma": 0.5
				}
			]
		}
	];
	// Set map height to 100% ----------------------------------------------------------------------------------------------

	var $body = $('body');
	if( $body.hasClass('map-fullscreen') ) {
		if( $(window).width() > 768 ) {
			$('.map-canvas').height( $(window).height() - $('.header').height() );
		}
		else {
			$('.map-canvas #map').height( $(window).height() - $('.header').height() );
		}
	}
	$(document).ready(function(){
		var lat = -6.192375;
		var long = 106.797700;
    	var mapCenter = new google.maps.LatLng(lat,long);
		var mapOptions = {
			zoom: 15,
			center: mapCenter,
			disableDefaultUI: true,
			scrollwheel: false,
			styles: mapStyles,
			panControl: false,
			zoomControl: true,
			draggable: true
		};
		var mapElement = document.getElementById('map-detail');
		var map = new google.maps.Map(mapElement, mapOptions);
		icon = '<img src="{{ asset('logo.png') }}">';
		
		// Google map marker content -----------------------------------------------------------------------------------

		var markerContent = document.createElement('DIV');
		markerContent.innerHTML =
			'<div class="map-marker">' +
				'<div class="icon">' +
				icon +
				'</div>' +
			'</div>';

		// Create marker on the map ------------------------------------------------------------------------------------

		var marker = new RichMarker({
			position: new google.maps.LatLng(lat,long),
			map: map,
			draggable: false,
			content: markerContent,
			flat: true
		});

		marker.content.className = 'marker-loaded';
    });

	function initializeOwl(_rtl){
            $.getScript( "{{ asset(null) }}js/owl.carousel.min.js", function( data, textStatus, jqxhr ) {
                if ($('.owl-carousel').length > 0) {
                    if ($('.carousel-full-width').length > 0) {
                        setCarouselWidth();
                    }
                    $(".carousel.wide").owlCarousel({
                        rtl: _rtl,
                        items: 1,
                        responsiveBaseWidth: ".slide",
                        nav: true,
                        navText: ["",""]
                    });
                    $(".item-slider").owlCarousel({
                        rtl: _rtl,
                        items: 1,
                        autoHeight: true,
                        responsiveBaseWidth: ".slide",
                        nav: false,
                        callbacks: true,
                        URLhashListener: true,
                        navText: ["",""]
                    });
                    $(".list-slider").owlCarousel({
                        rtl: _rtl,
                        items: 1,
                        responsiveBaseWidth: ".slide",
                        nav: true,
                        navText: ["",""]
                    });
                    $(".testimonials").owlCarousel({
                        rtl: _rtl,
                        items: 1,
                        responsiveBaseWidth: "blockquote",
                        nav: true,
                        navText: ["",""]
                    });

                    $('.item-gallery .thumbnails a').on('click', function(){
                        $('.item-gallery .thumbnails a').each(function(){
                            $(this).removeClass('active');
                        });
                        $(this).addClass('active');
                    });
                    $('.item-slider').on('translated.owl.carousel', function(event) {
                        var thumbnailNumber = $('.item-slider .owl-item.active img').attr('data-hash');
                        $( '.item-gallery .thumbnails #thumbnail-' + thumbnailNumber ).trigger('click');
                    });
                    return false;
                }
            });
        }
    $(window).load(function(){
        var rtl = false; // Use RTL
        initializeOwl(rtl);
    }); 

    

</script>
@if($errors->any())

		{!! flashValidation($errors->all()) !!}	

	@endif

	@if(Session::has('success'))

		{!! flash('Sukses',Session::get('success'),'success') !!}

	@endif

@endsection
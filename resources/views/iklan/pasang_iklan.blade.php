@extends('layouts.layout')

@section('content')
<style>
	.fieldsetmaps{
		background:yellow;
		margin-left:50px;
		display:none;
	}
	.fieldsetmaps input {
		height:10px;
		width:100px;
	}
    .map_canvas{
		display:block;
		width:100%;
		height:300px;
	} 
</style>
 
<div class="bg_content pagestyle pagecontent">
	<div class="container search-bar horizontal collapse in">
		@include('layouts.search')
	</div> 
	<div class="container">
		<header>
			<h1 class="page-title" style="text-align: center; font-size: 24px;">Pasang Iklan</h1>
		</header>
		
		
		<div class="row">
                        <!--Content-->
                        {!! Form::model($model,['class' => 'pasangiklan' ,'files' => true]) !!}
                            <div class="col-md-8">
                                
                                    <div class="form-group large">
                                        <label for="title">Judul *</label>
                                        {!! Form::text('judul' , null , ['class' => 'form-control','id'=>'judul']) !!}
                                    </div>
                                <!--Foto-->
								<?php /*	
									<div class="form-group">
										<label for="file-submit">Upload Foto *</label>
										<div id="file-submit" class="dropzone dz-clickable">
											<input name="file"  type="file" multiple="">
											<div class="dz-default dz-message"><span>Klik atau tarik foto ke sini</span></div>
										</div>
                                    </div>
                                */ ?>
                               
                               <input name="file[]"  type="file" multiple="">
                                
                                <!--end Foto-->
                                
                                    <div class="form-group large">
                                        <label for="kategori">Pilih Kategori *</label> 
                                        {!! Form::select('category_id',$categories,null,['class' => 'selectpicker form-control' ,'data-live-search' => true]) !!}
                                    </div>
                                    <!-- /.form-group -->
                                 
                                 	<div class="form-group large">
                                        <label for="title">Harga *</label>
                                        {!! Form::text('harga' , null , ['class' => 'form-control','id'=>'kategori']) !!}
                                    </div>

                                    <div class="form-group large">
                                        <label for="title">Deskripsikan iklan anda *</label>
                                        {!! Form::textarea('deskripsi' , null , ['id' => 'deskripsi' ,'style' => 'resize:vertical;','class' => 'form-control']) !!}
                                    </div>
                                 
                                    <div class="form-group large">
                                        <label for="title">Provinsi *</label>
                                        {!! Form::text('provinsi' , null , ['class' => 'form-control','id'=>'kategori']) !!}
                                    </div>
                                 
                                    <div class="form-group large">
                                        <label for="title">Kota *</label>
                                        {!! Form::text('kota' , null , ['class' => 'form-control','id'=>'sub-kategori']) !!}
                                    </div>
									
									<div class="form-group large fieldsmap">
										<label for="geocomplete"><input type="checkbox" class="pull-left" id="map-show" name="map-show" checked>Tampilkan Iklan Saya di Peta</label>
										<div class="clear"></div>
										<input id="geocomplete" class="geocomplete" type="text" placeholder="Type in an address" value="Empire State Bldg" />
										<button id="find" class="find btn btn-default" type="button"><i class="fa fa-map-marker" aria-hidden="true"></i></button>
										<div class="map_canvas"></div>
									</div>
                                 
                                    <div class="form-group">
                                        
										 <fieldset class="fieldsetmaps">
											<h3>Address-Details</h3>

											<label>Name</label>
											<input name="name" type="text" value="">
 
											<label>Latitude</label>
											<input name="lat" type="text" value="">

											<label>Longitude</label>
											<input name="lng" type="text" value="">

											<label>Location</label>
											<input name="location" type="text" value="">

											<label>Location Type</label>
											<input name="location_type" type="text" value="">

											<label>Formatted Address</label>
											<input name="formatted_address" type="text" value=""> 

											<label>Route</label>
											<input name="route" type="text" value="">

											<label>Street Number</label>
											<input name="street_number" type="text" value="">

											<label>Postal Code</label>
											<input name="postal_code" type="text" value="">

											<label>Locality</label>
											<input name="locality" type="text" value="">

											<label>Sub Locality</label>
											<input name="sublocality" type="text" value="">

											<label>Country</label>
											<input name="country" type="text" value="">

											<label>Country Code</label>
											<input name="country_short" type="text" value="">

											<label>State</label>
											<input name="administrative_area_level_1" type="text" value="">

											<label>Place ID</label>
											<input name="place_id" type="text" value="">

											<label>ID</label>
											<input name="id" type="text" value="">

											<label>Reference</label>
											<input name="reference" type="text" value="">

											<label>URL</label>
											<input name="url" type="text" value=""> 
										  </fieldset>
                                    </div> 
                                <hr>
                            </div>
                            <!--/.col-md-9-->
                            <!--Informasi Pemasang-->
                            <div class="col-md-4 col-sm-9 informasi_pemasang">
                                <h3><i class="fa fa-info-circle"></i> Informasi Pemasang</h3>
                                    <div class="form-group">
                                        <label for="name">Nama Lengkap</label>
                                        {!! Form::text('nama' , null , ['class' => 'form-control']) !!}
                                    </div>
                                    <!--/.form-group-->
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        {!! Form::text('email' , null , ['class' => 'form-control']) !!}
                                    </div>
                                    <!--/.form-group-->
                                    <div class="form-group">
                                        <label for="pin-bb">Pin BB <img src="{{ asset(null) }}images/material/bbm.png" alt=""></label>
                                        {!! Form::text('pin_bb' , null , ['class' => 'form-control']) !!}
                                    </div>
                                    <!--/.form-group-->
                                    <div class="form-group">
                                        <label for="phone">No Handphone</label>
                                   		{!! Form::text('no_handphone' , null , ['class' => 'form-control']) !!}
                                    </div>
                                    <!--/.form-group-->
                                    <div class="form-group">
                                        <input type="checkbox" class="whatsapp pull-left" id="whatsapp" name="whatsapp" checked="">
                                        <label for="whatsapp">Saya bisa dihubungi via whatsapp <img src="{{ asset(null) }}images/material/whatsapp.png" onmousedown="return false;" alt="whatsapp"></label>
                                    </div>
                            </div>
                                <!--/.form-group-->
                            <div class="col-md-12">
                                <section>
                                    <div class="form-group pull-left">
                                        <input type="checkbox" class="terms pull-left" id="terms" name="agree">
                                        <label class="terms" style="padding-left:5px;">Saya setuju untuk memproses data iklan &amp; pribadi dengan <a href="#" class="link">Syarat Penggunaan Pasaran.com</a> sesuai dengan ketentuan hukum. </label>
                                    </div>
                                    <div class="form-group pull-right">
                                        <button type="submit" class="btn btn-default pull-right" id="submit">Pasang Iklan</button>
                                    </div>
                                        <!-- /.form-group -->
                                </section>
                            </div>
                        {!! Form::close() !!}
                        <!-- /.col-md-3-->
                    </div>
		
		
	</div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
@endsection

@section('script')
	
	@if($errors->any())

		{!! flashValidation($errors->all()) !!}	

	@endif

	@if(Session::has('success'))

		{!! flash('Sukses',Session::get('success'),'success') !!}

	@endif

@endsection
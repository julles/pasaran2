@extends('layouts.layout')

@section('content')
<link rel="stylesheet" href="{{ asset(null) }}css/user.style.css" type="text/css">
<link rel="stylesheet" href="{{ asset(null) }}css/style.css" type="text/css">
<style>
    .logo{
        width:250px;
    }
    
</style>

<div class="bg_content pagestyle  ">
	<div class="container search-bar horizontal collapse in">
		@include('layouts.search')
	</div>
</div>
			<div id="page-content">
                <section id="image">
                    <div class="container">
                        <div class="col-md-9 col-sm-offset-2">
                            <div class="text-banner">
                                <figure>
                                    <img src="{{ asset(null) }}images/marker.png" alt="">
                                </figure>
                                <div class="description">
                                    <h2>{{ $about->title }}</h2>
                                    {!! $about->description !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.container-->
                    <div class="background">
                       <img src="{{ asset(null) }}images/pasaran-bg.png" alt="Pasaran.com">>
                    </div>
                    <!--/.bakcground-->
                </section>
                <section class="block background-color-grey-dark" id="features">
                    <div class="container">
                        <div class="row">
                        @foreach($section2 as $sec)
                           <div class="col-md-4 col-sm-4">
                               <div class="feature-box">
                                   <i class="{{ $sec->icon }}"></i>
                                   <div class="description">
                                       <h3>{{ $sec->title }}</h3>
                                       {!! $sec->description !!}
                                   </div>
                               </div>
                               <!--/.feature-box-->
                           </div>
                        @endforeach
                           
                       </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </section>
                <!--/.block-->
                <section class="block" id="the-team">
                    <div class="container">
                        <header class="no-border" style="text-align: center; padding-top: 30px; "><h1>Pasaran Team</h1></header>
                        <div class="row">
                        @foreach($team as $t)
                            <div class="col-md-3 col-sm-3">
                                <div class="member">
                                    <img src="{{ asset('contents/'.$t->image) }}" alt="">
                                    <h4>{{ $t->nama }}</h4>
                                    <figure>{{ $t->jabatan }}</figure>
                                    <hr>
                                    <div class="social">
                                        <a href="{{ $t->twitter }}" ><i class="fa fa-twitter"></i></a>
                                        <a href="{{ $t->facebook }}" ><i class="fa fa-facebook"></i></a>
                                        <a href="{{ $t->pinterest }}" ><i class="fa fa-pinterest"></i></a>
                                    </div>
                                </div>
                                <!--/.member-->
                            </div>
                        @endforeach    
                            <!--/.col-md-3-->
                        </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </section>
                <!--/.block-->
                <section class="block background-color-white" id="testimonials">
                    <div class="container">
                        <div class="owl-carousel testimonials">

                        @foreach($testimonial as $testi)
                            <blockquote>
                                <figure><img src="{{ asset('contents/'.$testi->image) }}" alt=""></figure>
                                <div class="description">
                                    <p>
                                        {{ $testi->testimonial }}
                                    </p>
                                    <footer>{{ $testi->nama }}</footer>
                                </div>
                            </blockquote>
                        @endforeach
                        </div>
                        <!--/.testimonials-->
                    </div>
                    <!--/.container-->
                </section>
                <!--/.testimonials-->
                <section id="partners" class="block">
                    <div class="container">
                        <header class="no-border"><h3>Partners</h3></header>
                        <div class="logos">
                        @foreach($partner as $p)
                            <div class="logo"><a href=""><img src="{{ asset('contents/'.$p->image) }}" alt=""></a></div>
                        @endforeach
                        </div>
                        <!--/.logos-->
                    </div>
                    <!--/.container-->
                </section>
                
            </div>
</div>

@endsection
@section('script')
<script>
	var $ = jQuery.noConflict();
    if( $('body').hasClass('navigation-fixed') ){
        $('.off-canvas-navigation').css( 'top', - $('.header').height() );
        $('#page-canvas').css( 'margin-top',$('.header').height() );
    }
	$(document).ready(function($) {
		$('.off-canvas-navigation header').css( 'line-height', $('.header').height() + 'px' );
		"use strict";
		$(document).bind('keypress', 'M', function(){ 
			$('.header .toggle-navigation').trigger('click');
			return false;
		});
	});
</script>
@endsection
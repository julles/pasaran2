# PASARAN DEVELOPMENT MAIN REPO

![alt tag](https://img.shields.io/badge/Version-DEV-red.svg)

### How to Install


A. Clone the project

```sh
	
	git clone https://julles@bitbucket.org/julles/pasaran2.git

```

B. Setting Database Connection in .env file

```sh
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=databaseName
	DB_USERNAME=username
	DB_PASSWORD=passwordname

```

C. composer install

D. Run Pasaran Console

```sh

php artisan pasaran:update

```

Default URL admin : http://yourapp.dev/admin-cp

Finish


Login User

|  Username  |      Password      |  Role |
|:--------:|:-------------:|------:|
|superadmin |  admin | Super Admin |



<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldIpAndAllowNullUserIdInIklanLikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('iklan_likes', function (Blueprint $table) {
            $table->dropForeign('iklan_likes_user_id_foreign');
            $table->integer('user_id')->unsigned()->nullable()->change();
            $table->string('ip')->index();
            $table->foreign('user_id')->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('iklan_likes', function (Blueprint $table) {
            $table->dropColumn('ip');
        });
    }
}

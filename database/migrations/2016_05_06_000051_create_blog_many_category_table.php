<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogManyCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_many_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('blog_id')->unsigned();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('blog_catgories')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('blog_id')->references('id')->on('blogs')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog_many_category');
    }
}

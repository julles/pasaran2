<?php

use Illuminate\Database\Seeder;


class MenuSeed extends Seeder
{
    public function run()
    {
        // iklan management
        \webarq::addMenu([ 
            'parent_id'     => null,
            'title'         => 'Iklan Management',
            'controller'    => '#',
            'slug'          => 'iklan-management',
            'order'         => 1,
        ],[]);

                \webarq::addMenu([ 
                    'parent_id'     => 'iklan-management',
                    'title'         => 'Category',
                    'controller'    => 'CategoryController',
                    'slug'          => 'category',
                    'order'         => '1'
                ],['index','create','update','delete']);

                \webarq::addMenu([ 
                    'parent_id'     => 'iklan-management',
                    'title'         => 'Iklan',
                    'controller'    => 'IklanController',
                    'slug'          => 'iklan',
                    'order'         => '2'
                ],['index','create','update','delete']);

                \webarq::updateMenu([ 
                    'parent_id'     => 'iklan-management',
                    'title'         => 'Iklan',
                    'controller'    => 'IklanController',
                    'slug'          => 'iklan',
                    'order'         => '2'
                ],['index','view','delete']);
                
                \webarq::addMenu([ 
                    'parent_id'     => 'iklan-management',
                    'title'         => 'Category Ordering',
                    'controller'    => 'CategoryOrderController',
                    'slug'          => 'category-ordering',
                    'order'         => '2'
                ],['index','create','delete']);
            
            //
                
            // Blog Management
            \webarq::addMenu([ 
                'parent_id'     => null,
                'title'         => 'Blog Management',
                'controller'    => '#',
                'slug'          => 'blog-management',
                'order'         => 1,
            ],[]);


                \webarq::addMenu([ 
                    'parent_id'     => 'blog-management',
                    'title'         => 'Category',
                    'controller'    => 'BlogCategoryController',
                    'slug'          => 'blog-category',
                    'order'         => '1'
                ],['index','create','delete','update']);

                \webarq::addMenu([ 
                    'parent_id'     => 'blog-management',
                    'title'         => 'Blog',
                    'controller'    => 'BlogController',
                    'slug'          => 'blog',
                    'order'         => '1'
                ],['index','create','delete','update','publish']);

                \webarq::addMenu([ 
                    'parent_id'     => 'blog-management',
                    'title'         => 'Faq',
                    'controller'    => 'FaqController',
                    'slug'          => 'faq',
                    'order'         => '2'
                ],['index','create','delete','update','publish']);

                \webarq::addMenu([ 
                    'parent_id'     => 'blog-management',
                    'title'         => 'Faq Listing',
                    'controller'    => 'FaqListingController',
                    'slug'          => 'faq-listing',
                    'order'         => '3'
                ],['index']);
        //
        
        // About Management
        \webarq::addMenu([ 
            'parent_id'     => null,
            'title'         => 'About Management',
            'controller'    => '#',
            'slug'          => 'about-management',
            'order'         => 4,
        ],[]);

            \webarq::addMenu([ 
                    'parent_id'     => 'about-management',
                    'title'         => 'About',
                    'controller'    => 'AboutController',
                    'slug'          => 'about',
                    'order'         => 1
                ],['index']);

                \webarq::addMenu([ 
                    'parent_id'     => 'about-management',
                    'title'         => 'About Section 2',
                    'controller'    => 'AboutSection2Controller',
                    'slug'          => 'about-section-2',
                    'order'         => 2
                ],['index','create','update','delete']);

                \webarq::addMenu([ 
                    'parent_id'     => 'about-management',
                    'title'         => 'Our Team',
                    'controller'    => 'OurTeamController',
                    'slug'          => 'our-team',
                    'order'         => 3
                ],['index','create','update','delete']);

                \webarq::addMenu([ 
                    'parent_id'     => 'about-management',
                    'title'         => 'Testimonial',
                    'controller'    => 'TestimonialController',
                    'slug'          => 'testimonial',
                    'order'         => 4
                ],['index','create','update','delete']);

                \webarq::addMenu([ 
                    'parent_id'     => 'about-management',
                    'title'         => 'Partners',
                    'controller'    => 'PartnerController',
                    'slug'          => 'partner',
                    'order'         => 5
                ],['index','create','update','delete']);

                \webarq::addMenu([ 
                    'parent_id'     => 'about-management',
                    'title'         => 'Syarat',
                    'controller'    => 'SyaratController',
                    'slug'          => 'syarat',
                    'order'         => 6
                ],['index','create','update','delete']);
        // 

    }
}

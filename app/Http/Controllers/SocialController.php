<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Social;
use App\User;
use Image;
use Auth;
use App\Models\LastLogin;
class SocialController extends Controller
{

	public $user;

	public function __construct(User $user,LastLogin $lastLogin)
	{
		$this->user = $user;
    $this->lastLogin = $lastLogin;
	}

  public function getLogin($param)
  {
  	return \Social::driver($param)->redirect();
  }

	public function getProsesSocial($param)
	{
		$user = Social::driver($param)->user();

		$email =  $user['email'];

		$cekEmail = $this->user->whereEmail($email)->count();

    		if($cekEmail > 0)
    		{
    			Auth::loginUsingId($this->user->whereEmail($email)->first()->id);
    		}else{
    			$avatar = $user->getAvatar();
    			$avatarName = randomImage().'.jpg';
    			$image = Image::make($avatar)->save(public_path('contents/'.$avatarName));

    			$newUser = $this->user->create([
    				'name'		=> $user['name'],
    				'email'		=> $email,
    				'role_id'	=> 88,
    				'status'	=> 'frontend',
    				'image'		=> $avatarName,
    			]);

    			Auth::loginUsingId($newUser->id);
      }
      $lastLogin = $this->lastLogin->create([
          'user_id'   => auth()->user()->id,
      ]);
	   return redirect('account/me');
	}
}

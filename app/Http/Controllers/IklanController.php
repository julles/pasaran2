<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Image;

use App\Models\Iklan as IklanMaster;
use App\Models\Category;
use App\Models\IklanImage;
use App\Models\Repositories\Iklan;
use App\User;
use App\Models\Message;
use App\Models\Percakapan;

class IklanController extends Controller
{
	public $model;

	public $category;

	public $image;

	public $user;

    public $message;

    public $repo;

	public function __construct(IklanMaster $model, Category $category,IklanImage $image,User $user,Iklan $repo,Message $message,Percakapan $percakapan)
	{
		$this->model = $model;
		$this->category = $category;
		$this->image = $image;
		$this->user = $user;
        $this->repo = $repo;
        $this->message = $message;
        $this->percakapan = $percakapan;
	}	

	public function categories()
	{
		//return $this->category->lists('title','id')->toArray();
        
        $category = new Backend\CategoryController(new \App\Models\Category);
        $combo = $category->parents();
        unset($combo['']);
        return $combo;
	}

    public function getPasangIklan()
    {
    	return view('iklan.pasang_iklan',[
    		'model'			=> $this->model,
    		'categories'	=> $this->categories(),
    	]);
    }

    public function insertUser($request)
    {
    	$user = $this->user->create([
    		'name'				=> $request->nama,
    		'role_id'			=> 88,
    		'email'				=> $request->email,
    		'status'			=> 'frontend',
    		'pin_bb'			=> $request->pin_bb,
    		'no_handphone'		=> $request->no_handphone,
    		'whatsapp'			=> !empty($request->whatsapp) ? 'yes' : 'no',
    		
    	]);

    	return $user;
    }

    public function insertIklan($request,$user)
    {
        $iklan = $this->model->create([
    		'user_id'			=> $user->id,
    		'judul'				=> $request->judul,
    		'category_id'		=> $request->category_id,
    		'deskripsi'			=> $request->deskripsi,
    		'provinsi'			=> $request->provinsi,
    		'kota'				=> $request->kota,
    		'longitude'			=> $request->lng,
    		'latitude'			=> $request->lat,
    	    'status'			=> 'p',
    	    'harga'				=> $request->harga,
            'slug'              => str_slug($user->id.'-'.str_random(3).' '.$request->judul),
            'gmap_address'      => $request->formatted_address,
        ]);
        
        return $iklan;
	}

    public function postPasangIklan(Request $request)
    {
    	$model = $this->model;

    	//$this->validate($request,$model->rules(),$model->messages());

        $cek = $this->user->whereEmail($request->email)->first();
    	if(!empty($cek->id))
        {
            $user = $cek;
        }else{
           $user = $this->insertUser($request); // simpan data user
        }
        
        $iklan = $this->insertIklan($request,$user); // simpan data iklan

    	// insert images
    	
    	$files = $request->file('file');

        if(!empty($files))
        {
            foreach($files as $file)
            {
                $imageName = randomImage().'.'.$file->getClientOriginalExtension();
                
                $this->image->create([
                    'iklan_id'      => $iklan->id,
                    'image'         => $imageName,
                ]);
                
                Image::make($file)->resize(555,415)->save(public_path('contents/'.$imageName));
            }
        }

    	//
    	

    	return redirect()->back()->withSuccess('Iklan telah disimpan'); 

    }

    public function lastLogin($model)
    {
        $lastLogin = $model->user->last()->orderby('created_at','desc')->first();
        if(!empty($lastLogin->id))
        {
            $lastLogin = waktuLalu($lastLogin->created_at);
        }else{
            $lastLogin = "";
        }
        return $lastLogin;
    }

    public function getViewIklan($slug)
    {
        $model = $this->model->whereSlug($slug)->firstOrFail();
        
        $lastLogin = $this->lastLogin($model);
        
        $this->repo->handleView($model->id); // eksekusi views
        
        $iklanTerkait = $this->repo->iklanTerkait($model)->limit(5)->get();

        return view('iklan.detail',compact('model','lastLogin','iklanTerkait'));
    }

    public function postKirimPesan(Request $request,$id)
    {
        $model = $this->message;
        $iklan = $this->model->findOrFail($id);

        if(auth()->check())
        {
            $this->validate($request,['pesan' => 'required']);
            $user = auth()->user();
            $message = $model->create([
                'iklan_id'  => $iklan->id,
                'user_id'   => $user->id,
                'nama'      => $user->name,
                'email'     => $user->email,
                'pesan'     => $request->pesan,
            ]);

            $this->percakapan->create([
                'message_id' => $message->id,
                'from'       => $user->id,
                'to'         => $iklan->user_id,
                'pesan'      => $request->pesan,         
            ]);

        }else{
            $this->validate($request,$model->rules());

            $model->create([
                'iklan_id'  => $iklan->id,
                'user_id'   => null,
                'nama'      => $request->nama,
                'email'     => $request->email,
                'pesan'     => $request->pesan,
            ]);
        }
        return redirect()->back()->withSuccess('Pesan telah dikirim ke penjual');
    }

    public function getLike()
    {
        if(request()->ajax())
        {
           $id = request()->get('iklanId');
           $this->repo->handleLike($id);
           return response()->json([
                'count' => count_likes(),
            ]);            
        }else{
           abort(404);
        }
    }
}

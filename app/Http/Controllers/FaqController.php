<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Faq;
use App\Models\Iklan;
use App\Models\BlogCategory;
use App\Models\Question;

class FaqController extends Controller
{
    public $model;

    public function __construct(Faq $model,Iklan $iklan , BlogCategory $blogCategory,Question $question)
    {
    	$this->model = $model;
    	$this->iklan = $iklan;
    	$this->blogCategory = $blogCategory;
    	$this->iklans = $this->iklan->whereStatus('y')->orderBy('created_at','desc')->limit(3)->get();
		$this->categories = $this->blogCategory->all();
        $this->question = $question;
	
    }

    public function getIndex()
    {
    	$faqs = $this->model->orderBy('order','asc')->whereStatus('y')->get();

    	return view('faq.index' , [
    		'faqs' => $faqs,
    		'iklans' => $this->iklans,
    		'categories' => $this->categories,
    	]);
    }

    public function postIndex(Request $request)
    {
        $model = $this->question;

        $this->validate($request , $model->rules());

        $model->create($request->all());

        return redirect()->back()->withSuccess('Data sent');
    }
}

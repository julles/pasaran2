<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Repositories\Iklan;
use App\Models\Category;

class HomeController extends Controller
{
	public $iklan;

    public $category;

    public function __construct(Iklan $iklan,Category $category)
    {
    	$this->iklan = $iklan;
        $category = $this->category;
    }

    public function getIndex()
    {
    	$iklan = $this->iklan;
    	return view('home.index',compact('iklan'));
    }
}

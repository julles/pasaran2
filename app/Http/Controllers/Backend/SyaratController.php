<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Table;
use Image;
use App\Models\Syarat;
class SyaratController extends Controller
{
    public function __construct(Syarat $model)
    {
    	$this->model = $model;
    }

    public function getData()
    {
    	$model = $this->model->select('id','title');

    	$tables = Table::of($model)

    	->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})	

    	->make(true);

    	return $tables;
    }

    public function getIndex()
    {
    	return view('backend.syarat.index');
    }

    public function getCreate()
    {
    	$model = $this->model;

    	return view('backend.syarat._form',compact('model'));
	}

	public function postCreate(Request $request)
	{
		
		$this->model->create($request->all());

		return redirect(urlBackendAction('index'))->withSuccess('Data has been saved');
	}

	public function getUpdate($id)
    {
    	$model = $this->model->findOrFail($id);

    	return view('backend.syarat._form',compact('model'));
	}

	public function postUpdate(Request $request,$id)
	{
		
		$this->model->findOrFail($id)->update($request->all());

		return redirect(urlBackendAction('index'))->withSuccess('Data has been updated');
	}

	public function getDelete($id)
    {
        $model = $this->model->findOrFail($id);

        $model->delete();

        return redirect(urlBackendAction('index'))->withSuccess('Data has been deleted');

    }

    public function getView($id)
    {
    	$model = $this->model->findOrFail($id);
    	$menu = injectModel('Menu');
    	return view('backend.syarat.view',compact('model','menu'));
    }

    public function postView(Request $request , $id)
    {
    	$model = $this->model->findOrFail($id);

    	$count = count($request->menu_action_id);

    	DB::beginTransaction();

    	try
    	{

    		$this->right->wheresyaratId($model->id)->delete();

    		for($a=0;$a<$count;$a++)
    		{
    			$this->right->create([
    				'syarat_id'			=> $model->id,
    				'menu_action_id'	=> $request->menu_action_id[$a],
    			]);
    		}

    		DB::commit();
    		
    		return redirect(urlBackendAction('index'))->withSuccess('Data has been updated');
    	
    	}catch(\Exception $e){

    		DB::rollback();
    	
    		return redirect(urlBackendAction('index'))->withInfo('Transaction Failed : '.$e->getMessage());
    	}
    }
}

<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AboutSection2;
use Table;

class AboutSection2Controller extends Controller
{
	public function __construct(AboutSection2 $model)
	{
		$this->model = $model;
	}

    public function getData()
    {
    	$model = $this->model->select('id','title','description');

    	$tables = Table::of($model)

    	->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})	

    	->make(true);

    	return $tables;
    }

    public function getIndex()
    {
    	return view('backend.about_section2.index');
    }

    public function getCreate()
    {
    	return view('backend.about_section2._form',[
    		'model'	=> $this->model,
    	]);
    }

    public function postCreate(Request $request)
    {
    	$inputs = $request->all();

    	$model = $this->model;

    	$model->create($inputs);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been saved');
	
    }

    public function getUpdate($id)
    {
    	return view('backend.about_section2._form',[
    		'model'	=> $this->model->find($id),
    	]);
    }

    public function postUpdate(Request $request,$id)
    {
    	$inputs = $request->all();

    	$model = $this->model->find($id);

    	$model->update($inputs);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been updated');
	
    }

    public function getDelete($id)
    {
    	$model = $this->model->destroy($id);
    	return redirect(urlBackendAction('index'))->withSuccess('Data has been deleted');
	
    }
}

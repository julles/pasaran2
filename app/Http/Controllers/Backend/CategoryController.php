<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
	public $model;

	public function __construct(Category $model)
	{
		$this->model = $model;
	}

	public function childs($id , $no)
	{
		$childs = $this->model
		//->where('id','!=',request()->segment(4))
		->whereCategoryId($id)->get();
		
		$hasil = [];

		$no = $no + 1;

		$strip = "";

		for($i=0;$i<=$no;$i++)
		{
			$strip.="-";
		}

		foreach($childs as $row)
		{
				$hasil[$row->id] = $strip.$row->title;

				$hasil += $this->childs($row->id,$no);
		}

		return $hasil;
	}

	public function parents()
	{
		$parents = $this->model
		//->where('id','!=',request()->segment(4))
		->where("category_id",null)->get();

		$hasil = [];

		$no = 0;

		foreach($parents as $row)
		{
			$hasil[$row->id] = $row->title;
			$hasil += $this->childs($row->id , $no);
		}

		return ['' => 'This Root Parent'] + $hasil;
	}

	public function handleInsert($request,$model)
	{
		if($request->category_id != '')
		{
			$model->category_id = $request->category_id;
		}

		$model->title = $request->title;
		$model->icon = $request->icon;
		$model->slug = str_slug($request->title);
		$model->save();
	}

    public function getIndex()
    {
    	$model = $this->model->whereCategoryId(null)->get();
    	return view('backend.iklanModule.category.index',compact('model'));
    }

    public function getCreate()
    {
    	$model = $this->model;
    	$parents = $this->parents();
    	return view('backend.iklanModule.category._form',compact('model','parents'));
    }

    public function postCreate(Request $request)
    {
    	$model = $this->model;

    	$this->validate($request,$model->rules());

    	$this->handleInsert($request,$model);
    	
    	return redirect(urlBackendAction('index'))->withSuccess('Data has been saved');
	}

    public function getUpdate($id)
    {
    	$model = $this->model->findOrFail($id);
    	$parents = $this->parents();
    	return view('backend.iklanModule.category._form',compact('model','parents'));
    }

    public function postUpdate(Request $request,$id)
    {
    	$model = $this->model->findOrFail($id);

    	$this->validate($request,$model->rules($id));

    	if($model->id == $request->category_id)
    	{
    		return redirect()->back()->withDanger('Check Your Input');
    	}
    	$this->handleInsert($request,$model);
    	
    	return redirect(urlBackendAction('index'))->withSuccess('Data has been update');
	}

	public function getDelete($id)
	{
		$model = $this->model->findOrFail($id);

		try
		{
			$model->delete();
			return redirect(urlBackendAction('index'))->withSuccess('Data has been deleted');

		}catch(\Exception $e){
			return redirect(urlBackendAction('index'))->withInfo('Data cannot be deleted');
		}
	}
}

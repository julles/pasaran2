<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Table;

use App\Models\Iklan;

class IklanController extends Controller
{
    public $model;

    public function __construct(Iklan $model)
    {
    	$this->model = $model;
    }

    public function getData()
    {
    	$model = $this->model->select('iklans.id','iklans.judul','iklans.views','iklans.status','users.name')
    		->join('users','users.id','=','iklans.user_id')
    		->orderBy('iklans.created_at','desc');

    	$tables = Table::of($model)
    	->addColumn('status' , function($model){
    		switch ($model->status) {
    			case 'p':
    				return 'Pending';
    			break;
    			case 'n':
    				return 'In Active';
    			break;
    			case 'y':
    				return 'Active';
    			break;
    			default:
    				return '';
    			break;
    		}
    	})
    	->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})	

    	->make(true);

    	return $tables;
    }

    public function getIndex()
    {
    	return view('backend.iklan.index');
    }

    public function getView($id)
    {
    	$model = $this->model->findOrFail($id);

    	return view('backend.iklan.view',compact('model'));
    }

    public function postView(Request $request,$id)
    {
    	$model = $this->model->findOrFail($id);

    	$model->update([
    		'status'	=> $request->status,
    	]);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been updated');

    }

    public function getDelete($id)
    {
    	$model = $this->model->findOrFail($id);

    	foreach($model->images as $row)
    	{
    		@unlink(public_path('contents/'.$row->image));
    	}

    	$model->delete();

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been deleted');
    }
}

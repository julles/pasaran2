<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\CategoryOrder;
use Table;

class CategoryOrderController extends Controller
{
	public $model;

    public function __construct(CategoryOrder $model)
    {
    	$this->model = $model;
    }

    public function getData()
    {
    	$model  = $this->model->select('category_orders.id','category_orders.order','categories.title')
    		->join('categories','categories.id','=','category_orders.category_id');
    	
    	$tables = Table::of($model)
    	->addColumn('orderna' , function($model){
    		return '<input type = "text" value = "'.$model->order.'" onkeyup = "changeOrder('.$model->id.',this.value)" />';
    	
    	})	
    	->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})	

    	->make(true);

    	return $tables;
    }

    public function categories()
    {
    	$category = new CategoryController(new \App\Models\Category);
    	$combo = $category->parents();
    	unset($combo['']);
    	return $combo;
    }

    public function getIndex()
    {
    	return view('backend.category_order.index');
    }

    public function getCreate()
    {
    	$model = $this->model;

    	$count = $model->count();

    	if($count > 6)
    	{
    		return redirect()->back()->withInfo('Category yang didaftarkan sudah lebih dari 6');
    	}

    	$categories = $this->categories();

    	return view('backend.category_order._form',compact('model','categories'));
    }

    public function postCreate(Request $request)
    {
    	$this->model->create($request->all());

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been saved');
	}

	public function getChangeOrder()
	{
		$id = request()->id;
		$order = request()->order;
		$model = $this->model->findOrFail($id);
		$model->update(['order' => $order]);
	}

	public function getDelete($id)
	{
		$model = $this->model->findOrFail($id);

		$model->delete();

		return redirect(urlBackendAction('index'))->withSuccess('Data has been deleted');
	
	}
}

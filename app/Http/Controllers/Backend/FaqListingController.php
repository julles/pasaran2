<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Question;

class FaqListingController extends Controller
{
    public $model;

    public function __construct(Question $model)
    {
    	$this->model = $model;
    }

    public function getData()
    {
    	$model = $this->model->select('id','email','question')->orderBy('created_at','desc');

    	$data = \Table::of($model)->make(true);

    	return $data;
    }

    public function getIndex()
    {
    	return view('backend.faq_listing.index');
    }
}

<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\BlogCategory;
use Table;

class BlogCategoryController extends Controller
{
	public $model;

    public function __construct(BlogCategory $model)
    {
    	$this->model = $model;
    }

    public function getData()
    {
    	$model = $this->model->select('id','title');

    	$table = Table::of($model)

    	->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})	

    	->make(true);
    	
    	return $table;
    }

    public function getIndex()
    {
    	return view('backend.blog_category.index');
    }

    public function handleInputs($request)
    {
    	$inputs = $request->all();

    	$inputs['slug'] = str_slug($request->title);

    	return $inputs;
    }

    public function getCreate()
    {
    	return view('backend.blog_category._form',[
    		'model'	=> $this->model,
    	]);
    }

    public function postCreate(Request $request)
    {
    	$model = $this->model;

    	$this->validate($request,$model->rules());

    	$inputs = $this->handleInputs($request);
		
		$model->create($inputs);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been saved');
    }

    public function getUpdate($id)
    {
    	return view('backend.blog_category._form',[
    		'model'	=> $this->model->findOrFail($id),
    	]);
    }

    public function postUpdate(Request $request,$id)
    {
    	$model = $this->model->findOrFail($id);

    	$this->validate($request,$model->rules());

    	$inputs = $this->handleInputs($request);
		
		$model->update($inputs);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been updated');
    }

    public function getDelete($id)
    {
    	$model = $this->model->findOrFail($id);

    	try
    	{
    		$model->delete();

    		return redirect(urlBackendAction('index'))->withSuccess('Data has been deleted');

    	}catch(\Exception $e){
    		return redirect()->back()->withInfo('Data tidak bisa di hapus , Data ini masih digunakan oleh data lain');
    	}
    }
}

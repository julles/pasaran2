<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Testimonial;
use Table;
use Image;

class TestimonialController extends Controller
{
    public function __construct(Testimonial $model)
    {
    	$this->model = $model;
    }

    public function getData()
    {
    	$data = $this->model->select('id','nama');

    	$table = Table::of($data)
	    	->addColumn('action' , function($model){
	    		$status = $model->status == 'y' ? true : false;
            	return \webarq::buttons($model->id , [] , $status);
	    	})
	    	->make(true);

	    return $table;
    }

    public function getIndex()
    {
    	return view('backend.testimonial.index');	
    }

    public function getCreate()
    {
    	$model = $this->model;

    	return view('backend.testimonial._form',[
    		'model'	=> $model,
    	]);
    }

    public function postCreate(Request $request)
    {
    	$model = $this->model;

    	$inputs = $this->handleInput($request,$model);
    	
    	$model->create($inputs);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been saved');
	}

	public function getUpdate($id)
    {
    	$model = $this->model->findOrFail($id);

    	return view('backend.testimonial._form',[
    		'model'	=> $model,
    	]);
    }

    public function postUpdate(Request $request,$id)
    {
    	$model = $this->model->findOrFail($id);

    	$inputs = $this->handleInput($request,$model);
    	
    	$model->update($inputs);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been updated');
	
    }

	
    
    public function handleInput($request,$model)
    {
    	$inputs = $request->all();

    	$image = $request->file('image');

    	if(!empty($image))
    	{
			unlinkContents($model->image);

			$name = randomImage().'.'.$image->getClientOriginalExtension();

			Image::make($image)->resize(156,156)->save(public_path('contents/thumbnails/'.$name));

			Image::make($image)->resize(156,156)->save(public_path('contents/'.$name));

			$inputs['image'] = $name;
    	}else{
    		$inputs['image'] = $model->image;
    	}
		
		return $inputs;
    }

    public function getDelete($id)
    {
        $model = $this->model->findOrFail($id);

        unlinkContents($model->image);
        
        $model->delete();

        return redirect(urlBackendAction('index'))->withSuccess('Data has been deleted');
    }
}

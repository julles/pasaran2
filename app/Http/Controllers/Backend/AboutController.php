<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\About;

class AboutController extends Controller
{
    public function __construct(About $model)
    {
    	$this->model = $model;
    }

    public function getIndex()
    {
    	return view('backend.about._form',[
    		'model'	=> $this->model->find(1),
    	]);
    }

    public function postIndex(Request $request)
    {
    	$inputs = $request->all();

    	$model = $this->model->find(1);

    	$model->update($inputs);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been updated');
	
    }
}

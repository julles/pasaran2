<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Table;

use App\Models\Faq;

class FaqController extends Controller
{
    public $model;

    public function __construct(Faq $model)
    {
    	$this->model = $model;
    }

    public function getData()
    {
    	$data = $this->model->select('id','question','status','order');

    	$table = Table::of($data)
	    	->addColumn('action' , function($model){
	    		$status = $model->status == 'y' ? true : false;
            	return \webarq::buttons($model->id , [] , $status);
	    	})
	    	->make(true);

	    return $table;
    }

    public function getIndex()
    {
    	return view('backend.faq.index');	
    }

    public function getCreate()
    {
    	$model = $this->model;

    	return view('backend.faq._form' , [
    		'model'	=> $model,
    	]);
    }

    public function postCreate(Request $request)
    {
    	$model = $this->model;

    	$this->validate($request,$model->rules());

    	$insert = $model->create($request->all());

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been saved');
	}

	public function getUpdate($id)
    {
    	$model = $this->model->findOrFail($id);

    	return view('backend.faq._form' , [
    		'model'	=> $model,
    	]);
    }

    public function postUpdate(Request $request , $id)
    {
    	$model = $this->model->findOrFail($id);

    	$this->validate($request,$model->rules());

    	$insert = $model->update($request->all());

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been updated');
	}


	public function getPublish($id)
    {
        $model = $this->model->findOrFail($id);

        if($model->status == 'y')
        {
            $status = 'n';
            $msg = ' Data has been Un Published';
        
        }else{
            $status = 'y';
            $msg = ' Data has been Published';
        }

        $model->update([
            'status' => $status,
        ]);

        return redirect(urlBackendAction('index'))->withSuccess($msg);
    }

    public function getDelete($id)
    {
    	$this->model->destroy($id);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been deleted');
	
    }
}

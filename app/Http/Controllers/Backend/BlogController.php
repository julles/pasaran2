<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Table;
use Image;

use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\BlogManyCategory;

class BlogController extends Controller
{
    public $model;

    public $blogManyCategory;

    public function __construct(Blog $model,BlogManyCategory $blogManyCategory)
    {
    	$this->model = $model;

    	$this->blogManyCategory = $blogManyCategory;

    	$this->categories = BlogCategory::lists('title','id')->toArray();
    }

    public function getData()
    {
    	$data = $this->model->select('id','title','status');

    	$table = Table::of($data)
	    	->addColumn('action' , function($model){
	    		$status = $model->status == 'y' ? true : false;
            	return \webarq::buttons($model->id , [] , $status);
	    	})
	    	->make(true);

	    return $table;
    }

    public function getIndex()
    {
    	return view('backend.blog.index');	
    }

    public function getCreate()
    {
    	$model = $this->model;

    	$selected = $this->handleSelected($model);

    	return view('backend.blog._form',[
    		'model'	=> $model,
    		'categories'=>$this->categories,
    		'selected'	=> $selected,
    	]);
    }

    public function postCreate(Request $request)
    {
    	$model = $this->model;

    	$this->validate($request,$model->rules());

    	$inputs = $this->handleInput($request,$model);
    	
    	$newBlog = $model->create($inputs);

    	$blogCategory = $this->hanldeCategory($request->category_id,$newBlog->id);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been saved');
	}

	public function getUpdate($id)
    {
    	$model = $this->model->findOrFail($id);

    	$selected = $this->handleSelected($model);

    	return view('backend.blog._form',[
    		'model'	=> $model,
    		'categories'=>$this->categories,
    		'selected'	=> $selected,
    	]);
    }

    public function postUpdate(Request $request,$id)
    {
    	$model = $this->model->findOrFail($id);

    	$this->validate($request,$model->rules());

    	$inputs = $this->handleInput($request,$model);
    	
    	$newBlog = $model->update($inputs);

    	$blogCategory = $this->hanldeCategory($request->category_id,$id);

    	return redirect(urlBackendAction('index'))->withSuccess('Data has been updated');
	}

	public function handleSelected($model)
	{
		$count = count($model->categories);

		if($count > 0)
		{
			$arr = [];

			foreach($model->categories as $row)
			{
				$arr[] = $row->id;
			}

			return $arr;

		}else{
			return null;
		}

	}

    public function hanldeCategory($categories,$blogId)
    {
    	$this->blogManyCategory->whereBlogId($blogId)->delete();

    	$datas = [];

    	foreach($categories as $category)
    	{
    		$datas[] = [
    			'category_id'	=> $category,
    			'blog_id'		=> $blogId,
    		];
    	}

    	$this->blogManyCategory->insert($datas);
    }

    public function handleInput($request,$model)
    {
    	$inputs = $request->all();

    	unset($inputs['category_id']);

    	$image = $request->file('image');

    	if(!empty($image))
    	{
			unlinkContents($model->image);

			$name = randomImage().'.'.$image->getClientOriginalExtension();

			Image::make($image)->resize(200,100)->save(public_path('contents/thumbnails/'.$name));

			Image::make($image)->resize(849,249)->save(public_path('contents/'.$name));

			$inputs['image'] = $name;
    	}else{
    		$inputs['image'] = $model->image;
    	}


    	$inputs['slug'] = str_slug($request->title);

   		return $inputs;
    }

    public function getDelete($id)
    {
        $model = $this->model->findOrFail($id);

        unlinkContents($model->image);
        
        $model->delete();

        return redirect(urlBackendAction('index'))->withSuccess('Data has been deleted');
    }

    public function getPublish($id)
    {
        $model = $this->model->findOrFail($id);

        if($model->status == 'y')
        {
            $status = 'n';
            $msg = ' Data has been Un Published';
        
        }else{
            $status = 'y';
            $msg = ' Data has been Published';
        }

        $model->update([
            'status' => $status,
        ]);

        return redirect(urlBackendAction('index'))->withSuccess($msg);
    }	
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;
use Image;
use App\Models\Iklan;
use App\Models\Category;
use App\Models\IklanImage;
use App\Models\Repositories\Account;
use App\Models\LastLogin;
use App\Models\Message;
use App\Models\Percakapan;

class AccountController extends Controller
{
	public $user;

    public $iklan;

    public $category;

    public $image;

    public $paginate;

	public function __construct(User $user,Iklan $iklan, Category $category,IklanImage $image,Account $repo,LastLogin $lastLogin,Message $message,Percakapan $percakapan)
	{
		$this->user = $user;
        $this->iklan = $iklan;
        $this->category = $category;
        $this->image = $image;
        $this->repo = $repo;
        $this->lastLogin = $lastLogin;
        $this->message = $message;
        $this->percakapan = $percakapan;
        $this->paginate = 5;
	}

    public function getMyAccount()
    {
    	return view('account.login');
    }

    public function postMyAccount(Request $request)
    {

    	$rules = [
    		'email'		=> 'required|email',
    		'password'	=> 'required',
    	];

    	$messages = [
    		'email.required'			=> 'email tidak boleh kosong',
    		'password.required'			=> 'password tidak boleh kosong',
    		'email.email'				=> 'Format Email tidak valid',
    	];

    	$this->validate($request,$rules,$messages);

    	$credentials = [
    		'email'		=> $request->email,
    		'password'	=> $request->password,
    	];

    	if(!Auth::attempt($credentials))
    	{
    		return redirect()->back()->withFailed('Akun tidak ditemukan')->withInput();
    	}else{

            $lastLogin = $this->lastLogin->create([
                'user_id'   => auth()->user()->id,
            ]);

    		return redirect('account/me');
    	}
    }

    public function getDaftar()
    {
    	return view('account.daftar',[
    		'model'	=> $this->user,
    	]);
    }

    public function postDaftar(Request $request)
    {
    	$rules = [
    		'email'		=> 'required|email|unique:users',
    		'password'	=> 'required',
    		'verify_password'	=> 'required|same:password',
    	];

    	$messages = [
    		'email.required'			=> 'email tidak boleh kosong',
    		'password.required'			=> 'password tidak boleh kosong',
    		'verify_password.required'	=> 'Verify Password tidak boleh kosong',
    		'email.email'				=> 'Format Email tidak valid',
    		'email.unique'				=> 'E-mail sudah ada',
    		'verify_password.same'		=> 'Password tidak sama!',
    	];

    	$this->validate($request,$rules,$messages);

    	$newUser = $this->user->create([
    		'role_id'		=> 88,
    		'email'			=> $request->email,
    		'password'		=> \Hash::make($request->password),
    		'status'		=> 'frontend',
    	]);
        
        \Auth::loginUsingId($newUser->id);
        
        $lastLogin = $this->lastLogin->create([
                'user_id'   => auth()->user()->id,
        ]);

    	return redirect('account/me')->withSuccess('Selamat anda berhasil mendaftar silahkan login');
    }


    public function getMe()
    {
        if(Auth::check())
        {
            $model = $this->user->findOrFail(auth()->user()->id);

            return view('account.me_profile',[
                'model' => $model,
            ]);

        }else{
            abort(404);
        }        
    }


    public function postMe(Request $request)
    {
        $model = $this->user->findOrFail(auth()->user()->id);

        $rules = [
            'name'      => 'required',
            'email'     =>  'required|email',
        ];

        $this->validate($request,$rules);

        $inputs = $request->all();


        $file = $request->file('image');

        if(!empty($file))
        {
            $imageName=randomImage().'.'.$file->getClientOriginalExtension();

            Image::make($file)->resize(200,200)->save(public_path('contents/'.$imageName));

            $inputs['image'] = $imageName;
        }

        !empty($inputs['whatsapp']) ? $inputs['whatsapp'] = 'yes' : $inputs['whatsapp'] = 'no';

        $model->update($inputs);

         return redirect()->back()->withSuccess('Data Telah di ganti');
    }

    public function postChangePassword(Request $request)
    {
        if(Auth::check())
        {
           $model = $this->user->findOrFail(auth()->user()->id);

           $rules = [
            'password'          => 'required|min:8',
            'verify_password'   => 'required|same:password',
           ];

           $this->validate($request,$rules);

           $model->update([
                'password'      => \Hash::make($request->password),
           ]);

           return redirect()->back()->withSuccess('Password Telah di ganti');

        }else{
            abort(404);
        }
    }

    public function getLogout()
    {
        Auth::logout();

        return redirect('/');
    }

    public function categories()
    {
        //return $this->category->lists('title','id')->toArray();
        
        $category = new Backend\CategoryController(new \App\Models\Category);
        $combo = $category->parents();
        unset($combo['']);
        return $combo;
    }

    public function getPasangIklan()
    {
       if(Auth::check())
       {
            $model = $this->iklan;
            return view('account.pasang_iklan',[
                'model' => $model,
                'categories'    => $this->categories(),
            ]); 
       }else{
            abort(404);
       }
    }

    public function insertIklan($request,$user)
    {
        $iklan = $this->iklan->create([
            'user_id'           => $user->id,
            'judul'             => $request->judul,
            'category_id'       => $request->category_id,
            'deskripsi'         => $request->deskripsi,
            'provinsi'          => $request->provinsi,
            'kota'              => $request->kota,
            'longitude'         => $request->lng,
            'latitude'          => $request->lat,
            'status'            => 'p',
            'harga'             => $request->harga,
            'slug'              => str_slug($user->id.'-'.str_random(3).' '.$request->judul),
            'gmap_address'      => $request->formatted_address,
        ]);
        
        return $iklan;
    }

    public function updateIklan($request,$user,$model)
    {
        $iklan = $model->update([
            'user_id'           => $user->id,
            'judul'             => $request->judul,
            'category_id'       => $request->category_id,
            'deskripsi'         => $request->deskripsi,
            'provinsi'          => $request->provinsi,
            'kota'              => $request->kota,
            'longitude'         => $request->lng,
            'latitude'          => $request->lat,
            'harga'             => $request->harga,
            'slug'              => str_slug($user->id.'-'.str_random(3).' '.$request->judul),
            'gmap_address'      => $request->formatted_address,
        ]);
        
        return $iklan;
    }

    public function postPasangIklan(Request $request)
    {
        $model = $this->iklan;

        $this->validate($request,[
            'agree' => 'required'
        ]);

        $iklan = $this->insertIklan($request,auth()->user()); // simpan data iklan

        // insert images
        
        $files = $request->file('file');

        if(!empty($files))
        {
            foreach($files as $file)
            {
                $imageName = randomImage().'.'.$file->getClientOriginalExtension();
                
                $this->image->create([
                    'iklan_id'      => $iklan->id,
                    'image'         => $imageName,
                ]);
                
                Image::make($file)->resize(555,415)->save(public_path('contents/'.$imageName));
            }
        }

        //
        return redirect('account/me')->withSuccess('Iklan telah disimpan'); 

    }

    public function getEditIklan($id)
    {
        if(Auth::check())
        {
            $model = $this->iklan->findOrFail($id);
            return view('account.pasang_iklan',[
                'model' => $model,
                'categories'    => $this->categories(),
            ]); 

        }else{
            abort(404);
        }
    }


    public function postEditIklan(Request $request,$id)
    {
        $model = $this->iklan->findOrFail($id);

        $this->validate($request,[
            'agree' => 'required',
        ]);

        $iklan = $this->updateIklan($request,auth()->user(),$model); // simpan data iklan

        // insert images
        
        // foreach($model->images as $old)
        // {
        //     @unlink(public_path('contents/'.$old->image));
        // }

        $files = $request->file('file');

        if(count($request->file) > 0)
        {
            foreach($files as $file)
            {
                if(!empty($file))
                {
                    $imageName = randomImage().'.'.$file->getClientOriginalExtension();
                    
                    $this->image->create([
                        'iklan_id'      => $model->id,
                        'image'         => $imageName,
                    ]);
                    
                    Image::make($file)->resize(555,415)->save(public_path('contents/'.$imageName));
                }
                    
            }
        }
        //
        return redirect('account/me')->withSuccess('Iklan telah disimpan'); 

    }

    public function getMyIklan($status = "")
    {
        if(Auth::check())
        {
            $iklans = $this->repo->allMyIklan(Auth()->user()->id,$status);
            return view('account/me_iklan',compact('iklans'));
        }else{
            abort(404);
        }
    }

    public function getStatus($id)
    {
        if(Auth::check())
        {   
            $model = $this->iklan->findOrFail($id);
            $status = $model->status == 'y' ? 'n' : 'y';
            $model->update([
                'status' => $status
            ]);

            return redirect()->back()->withSuccess('Status Iklan telah dirubah');

        }else{
            abort(404);
        }
    }

    public function getPercakapan($id)
    {
        $model = $this->message->whereUserId($id);
        $cek = $model->firstOrFail();
        $chats = $this->repo->chats($id)->get();
        return view('account.percakapan',compact('chats'));

    }

    public function postPercakapan(Request $request,$id)
    {
        $model = $this->message->whereUserId($id);
        $cek = $model->firstOrFail();

        $this->validate($request,['pesan' => 'required']);
        $user = auth()->user();
        $message = $this->message->create([
            'iklan_id'  => $cek->iklan_id,
            'user_id'   => $id,
            'nama'      => $user->name,
            'email'     => $user->email,
            'pesan'     => $request->pesan,
            'status'    => 'penjual',
        ]);

        if(auth()->user()->id == $cek->iklan->user_id)
        {
            $to = $id;
        }else{
            $to = $cek->iklan->user_id;
        }

        $this->percakapan->create([
                'message_id' => $message->id,
                'from'       => $user->id,
                'to'         => $to,
                'pesan'      => $request->pesan,         
        ]);

        return redirect()->back();  
    }

    public function getMyInbox()
    {
        $messages = $this->repo->allMessages()->paginate($this->paginate);
        
        $ifUser = function($email,$id,$return){
            $user = $this->user->whereEmail($email)->count();

            if($user > 0)
            {
                $url = url('account/percakapan/'.$id);
                $user = '';
            }else{
                $url = '';
                $user = '( Unknown User )';
            }

            return ${$return};
            
        };

        return view('account.inbox' , compact('messages','ifUser'));
    }

    public function getMyInboxAll()
    {
        $messages = $this->repo->inboxes()->paginate($this->paginate);
        $ifUser = function($email,$id,$return){
            $user = $this->user->whereEmail($email)->count();

            if($user > 0)
            {
                $url = url('account/percakapan/'.$id);
                $user = '';
            }else{
                $url = '';
                $user = '( Unknown User )';
            }

            return ${$return};
            
        };

        return view('account.inbox' , compact('messages','ifUser'));
    }

    public function getSent()
    {
        $messages = $this->repo->sentes()->paginate($this->paginate);
       
        $ifUser = function($email,$id,$return){
            $user = $this->user->whereEmail($email)->count();

            if($user > 0)
            {
                $url = url('account/percakapan/'.$id);
                $user = '';
            }else{
                $url = '';
                $user = '( Unknown User )';
            }

            return ${$return};
            
        };

        return view('account.inbox' , compact('messages','ifUser'));
    }

    public function getDeleteOne($id)
    {
        $model = $this->image->findOrFail($id);

        @unlink(asset('contents/'.$model->image));

        $model->delete();

        return redirect()->back();
    }

}

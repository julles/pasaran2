<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Syarat;
use App\Models\Iklan;
use App\Models\BlogCategory;
class SyaratController extends Controller
{
    public function __construct(Syarat $model,Iklan $iklan,BlogCategory $blogCategory)
    {
    	$this->model = $model;
    	$this->iklan = $iklan;
    	$this->iklans = $this->iklan->whereStatus('y')->orderBy('created_at','desc')->limit(3)->get();
		$this->blogCategory = $blogCategory;
		$this->categories = $this->blogCategory->all();
    }

    public function index()
    {
    	$model = $this->model->all();

    	return view('syarat.index' , [
    		'model' => $model,
    		'iklans' => $this->iklans,
    		'categories' => $this->categories,
    	]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\About;
use App\Models\AboutSection2;
use App\Models\AboutTeam;
use App\Models\Testimonial;
use App\Models\Partner;

class TentangController extends Controller
{
    public function __construct(About $about, AboutSection2 $section2,AboutTeam $team,Testimonial $testimonial,Partner $partner)
    {
    	$this->about = $about;
    	$this->section2 = $section2;
    	$this->team = $team;
    	$this->testimonial = $testimonial;
    	$this->partner = $partner;
    }

    public function index()
    {
    	return view('tentang.tentang' , [
    		'about' => $this->about->find(1),
    		'section2'	=> $this->section2->limit(3)->get(),
    		'team'	=> $this->team->limit(4)->get(),
    		'testimonial'	=> $this->testimonial->all(),
    		'partner'	=> $this->partner->all(),
    	]);
    }
}

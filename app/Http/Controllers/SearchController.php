<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Repositories\Iklan;
use App\Models\Category;

class SearchController extends Controller
{
	public $iklan;
    public $paginate;
    public $category;

	public function __construct(Iklan $iklan,Category $category)
	{
		$this->iklan = $iklan;
        $this->paginate = 16;
        $this->category = $category;
	}

    public function getIndex()
    {
        // semua paramter get 

    	$location = request()->get('location');

        $category_id = request()->get('category_id');

    	$keyword = request()->get('keyword');

        $view = request()->get('view');

        //
    	
        // query search
        
        $model = $this->iklan->search($keyword,$location)
        
        ->orderBy('created_at','desc')
        
        ->paginate($this->paginate); 
        
        //
        
        // Pagination dengan membawa semua parameter pake appends
        
        $pagination = clone $model;
        
        $pagination = $pagination->appends(
            [
                'keyword' => $keyword,
                'location'=>$location,
                'category_id' => $category_id,
                'view'=>$view
        ]);

        //
        
        // menentukan path view berdasarkan varible $view

        switch ($view) {
            case 'grid':
                $page = 'index';
            break;

            case 'list':
                $page = 'list';
            break;

            case 'map':
                $page = 'map';
            break;
            
            default:
               $page = 'index';
            break;
        }

        //

        // rendering view

        return view('search.'.$page ,[
    		'model'	=> $model,
            'pagination' => $pagination,
    	]);
    }

    public function getCategory($id)
    {
        $category = $this->category->whereSlug($id)->firstOrFail();
        $model = $category->iklans()->paginate(20);
        $count = $category->iklans()->count();
        $pagination = clone $model;
        return view('search.category',compact('model','pagination','count'));
    }

    public function conditionLike()
    {
        $model = injectModel('IklanLike');

        if(\Auth::check())
        {
            $likes = $model->whereUserId(auth()->user()->id)->get();
        }else{
            $likes = $model->whereIp(request()->ip())->whereUserId(null)->get();
        }

        $arr = [];

        foreach($likes as $row)
        {
            $arr[] = $row->iklan_id;
        }

        return $arr;
    }

    public function getFavorits()
    {
        $iklanId = $this->conditionLike();
        $sql = $this->iklan->whereIn('id',$iklanId);
        $count = $sql->count();
        $model = $sql->paginate(20);
        $pagination = clone $model;

        return view('search.favorit',compact('model','pagination','count'));
    }
}

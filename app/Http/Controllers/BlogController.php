<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Blog;
use App\Models\Iklan;
use App\Models\BlogCategory;

class BlogController extends Controller
{

	public $model;

	public $iklan;

	public $blogCategory;

	public function __construct(Blog $model,Iklan $iklan,BlogCategory $blogCategory)
	{
		$this->model = $model;
		$this->iklan = $iklan;
		$this->blogCategory = $blogCategory;
		$this->iklans = $this->iklan->whereStatus('y')->orderBy('created_at','desc')->limit(3)->get();
		$this->categories = $this->blogCategory->all();
	}

    public function getIndex()
    {
    	$model = $this->model->whereStatus('y')->paginate(10);
    	
    	return view('blog.index' , [
    		'lists' => $model,
    		'pagination' => clone $model,
    		'iklans' => $this->iklans,
    		'categories' => $this->categories,
    	]);
    }

    public function getCategory($slug)
    {
    	$category = $this->blogCategory->whereSlug($slug)->firstOrFail();

    	$model = $category->blogs()->paginate(10);
    	
    	return view('blog.index' , [
    		'lists' => $model,
    		'pagination' => clone $model,
    		'iklans' => $this->iklans,
    		'categories' => $this->categories,
    	]);
    }

    public function getView($slug)
    {
    	$model = $this->model->whereSlug($slug)->firstOrFail();
    	
    	return view('blog.detail' , [
    		'list' => $model,
    		'iklans' => $this->iklans,
    		'categories' => $this->categories,
    	]);
    }
}

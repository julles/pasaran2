<?php
carbon()->setLocale('id');
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    
	// Backend Area
		Route::controller('login','Backend\LoginController');

		Route::get('admin-cp' , function(){
			return redirect('login');
		});

		if(request()->segment(1) == webarq()->backendUrl)
		{
			include __DIR__.'/backendRoutes.php';
		}
	//
    
	// Frontend Area
		Route::get('syarat-dan-ketentuan','SyaratController@index');
		Route::get('tentang-kami','TentangController@index');
		Route::controller('faq','FaqController');
		Route::controller('blog','BlogController');
		Route::controller('social','SocialController');
		Route::controller('account','AccountController');
		Route::controller('iklan','IklanController');
		Route::controller('search','SearchController');
		Route::controller('/','HomeController');

	//

    

});

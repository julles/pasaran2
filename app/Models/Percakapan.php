<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Message;

class Percakapan extends Model
{
    protected $table = 'percakapan';

    public $guarded = [];

    public function from()
    {
    	return $this->belongsTo(User::class,'from');
    }

    public function to()
    {
    	return $this->belongsTo(User::class,'to');
    }

    public function message()
    {
    	return $this->belongsTo(Messages::class,'message_id');
    }
}

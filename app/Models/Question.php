<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    public $guarded = [];

    public function rules()
    {
    	return [
    		'email'	=> 'required|email',
    		'question'	=> 'required',
    	];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Iklan;

class IklanImage extends Model
{
    protected $table = 'iklan_images';

    public $guarded = [];

    public function iklan()
    {
    	return $this->belongsTo(Iklan::class);
    }
}

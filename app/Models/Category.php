<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Iklan;
use App\Models\CategoryOrder;
class Category extends Model
{
    public $guarded = [];

    public function rules($id = "")
    {
    	if(!empty($id))
    	{
    		$unique = ",title,".$id;
    	}else{
    		$unique = "";
    	}

    	return [
    		'title'		=> 'required|unique:categories'.$unique,
    	];
    }

    public function messages()
    {
        //
    }

    public function parent()
    {
    	return $this->belongsTo(Category::class);
    }

    public function childs()
    {
    	return $this->hasMany(Category::class,'id');
    }

    public function iklans()
    {
        return $this->hasMany(Iklan::class,'category_id');
    }

    public function orders()
    {
        return $this->hasMany(CategoryOrder::class,'id');
    }

}

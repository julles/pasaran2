<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Blog;

class BlogCategory extends Model
{
    protected $table = 'blog_catgories';

    public $guarded = [];

    public function rules()
    {
    	return [
    		'title'	=> 'required',
    	];
    }

    public function blogs()
    {
    	return $this->belongsToMany(Blog::class,'blog_many_category','category_id','blog_id');
    }
}

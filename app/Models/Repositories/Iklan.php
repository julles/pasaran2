<?php namespace App\Models\Repositories;

/**
 * Author : Muhamad Reza AR
 * http://github.com/julles
 *
 * Berisi tarikan data dan actions iklan
 */

use App\Models\Iklan as IklanMaster;
use App\Models\IklanLike;
use DB;
use Auth;

class Iklan extends IklanMaster
{
	/**
	 * Menampilkan data terbaru iklan all record 
	 * bisa di batasi dengan limit
	 */

	public function terbaru($limit = "")
	{
		$model = $this->whereStatus('y')->orderBy('created_at','desc');
	
		if(is_numeric($limit))
		{
			$model = $model->limit($limit);
		}

		return $model->get();
	}

	/**
	 * Menampilkan data terpopuler 1 record
	 */

	public function populerOne()
	{
		$model = $this->whereStatus('y')->orderBy('views','desc')->first();

		return $model;
	}

	/**
	 * Menampilkan data terpopuler all records 
	 * bisa di batasi dengan limit
	 */

	public function populers($limit = "")
	{
		$model = $this->whereStatus('y')->orderBy('views','desc');
	
		if(is_numeric($limit))
		{
			$model = $model->limit($limit);
		}

		return $model->get();
	}

	public function populerToday($limit = "")
	{
		$y = date('Y');
		$m = date('m');
		$d = date('d');
		
		$model = $this->whereStatus('y')
		->whereRaw('YEAR(created_at) = "'.$y.'"')
		->whereRaw('MONTH(created_at) = "'.$m.'"')
		->whereRaw('DAY(created_at) = "'.$d.'"')
		->orderBy('views','desc');
	
		if(is_numeric($limit))
		{
			$model = $model->limit($limit);
		}

		return $model->get();
	}

	/**
	 * Menampilkan data iklan 1 record
	 * berdasarkan slug parameter url
	 */

	public function findSlug($slug)
	{
		$model = $this->model->whereStatus('y')->whereSlug($slug)->firstOrFail();

		return $model;
	}

	/**
	 * Menampilkan data favorits (paling banyak like nya)
	 * bisa di batasi dengan limit
	 */

	public function favorits($limit = "")
	{
		$model = $this->whereStatus('y')->orderBy('likes','desc');
	
		if(is_numeric($limit))
		{
			$model = $model->paginate($limit);
		}

		return $model->get();
	}

	/**
	 * Menghandle jika iklan di lihat oleh pengunjung
	 * ketentuan :
	 * hanya menyimpan data IP computer pengunjung pada 1 hari saja
	 */

	public function handleView($id)
	{
		$model = $this->findOrFail($id);

		$ip = request()->ip();

		$now = date("Y-m-d"); 

		 $cek = $model->views()->whereIp($ip)->whereRaw('DATE(created_at) = "'.$now.'"')->first();

		if(empty($cek->id))
		{
			DB::statement("UPDATE iklans SET views = views + 1 WHERE id = '".$id."'");
			$model = injectModel('iklanView')->create([
				'iklan_id'		=> $model->id,
				'ip'			=> $ip,
			]);
		}
	}

	/**
	 * Menghandle Search iklan by judul dan lokasi
	 */

	public function search($keyword = "",$location = "")
	{

		$model =  $this->whereStatus('y')

			->where('judul','like','%'.$keyword.'%');
		
		if(!empty($location))
		{
			$model = $model->where('provinsi','like','%'.$location.'%')
			
			->orWhere('kota','like','%'.$location.'%')
			
			->orWhere('gmap_address','like','%'.$location.'%');
		}	
			
		return $model;
			
	}

	/**
	 * Menghandle Iklan Terkait
	 */

	public function iklanTerkait($model)
	{
		$model = $this->whereStatus('y')->whereCategoryId($model->category_id)->where('id','!=',$model->id);
		return $model;
	}

	/**
	 * Menampilkan data iklan premium
	 */

	public function iklanPremiums()
	{
		return $this->whereStatus('premium');
	}

	public function handleLike($iklanId)
	{
		$iklan = $this->findOrFail($iklanId); // cek if iklan exists

		$ip = request()->ip();

		if(Auth::check())
		{
			$cekByUser = IklanLike::whereUserId(auth()->user()->id)->whereIklanId($iklanId)->first();
			if(empty($cekByUser->id))
			{
				IklanLike::create([
					'iklan_id'		=> $iklanId,
					'user_id'		=> auth()->user()->id,
					'ip'			=> $ip,
				]);

				$iklan->update([
					'likes' => $iklan->likes + 1,
				]);
			}else{
				$cekByUser->delete();
			}
		}else{
			
			$cekByIp = IklanLike::whereIp($ip)->whereUserId(null)->whereIklanId($iklanId)->first(); // cek like iklan by ip
			if(empty($cekByIp->id))
			{
				IklanLike::create([
					'iklan_id'		=> $iklanId,
					'user_id'		=> null,
					'ip'			=> $ip,
				]);
			}else{
				$cekByIp->delete();
			}
			
		}
	}
}
<?php namespace App\Models\Repositories;

use App\Models\Iklan;
use App\Models\Message;

class Account
{
	public $iklan;

	public $message;

	public function __construct()
	{
		$this->iklan = new Iklan;
		$this->message = new Message;
	}

	public function iklan($userId)
	{
		return $this->iklan->whereUserId($userId);
	}

	public function allMyIklan($userId,$status = "")
	{
		$iklans = $this->iklan($userId);

		if(!empty($status))
		{
			$iklans->whereStatus($status);
		}

		$iklans = $iklans->orderBy('created_at','desc')->get(); 
		
		return $iklans;
	}

	public function allMessages()
	{
		$model = $this->iklan->select('iklans.id','messages.nama','messages.pesan','messages.email','messages.user_id','percakapan.from','percakapan.to','users.email as email_iklan','users.name as nama_iklan')
			->join('messages','messages.iklan_id','=','iklans.id')
			->join('users','users.id','=','iklans.user_id')
			->leftJoin('percakapan','messages.id','=','percakapan.message_id')
			->where('users.id','=',auth()->user()->id)
			->orWhere('messages.user_id','=',auth()->user()->id)
			->orderBy('messages.created_at','desc');

		return $model;
	}

	public function chats($id)
	{
		$model = $this->iklan->select('iklans.id','messages.nama','messages.pesan','messages.email','messages.user_id','percakapan.from','percakapan.to','users.email as email_iklan','users.name as nama_iklan')
			->join('messages','messages.iklan_id','=','iklans.id')
			->join('users','users.id','=','iklans.user_id')
			->leftJoin('percakapan','messages.id','=','percakapan.message_id')
			->where('users.id','=',auth()->user()->id)
			->where('messages.user_id','=',$id)
			->orWhere('messages.user_id','=',auth()->user()->id)
			->orderBy('messages.created_at','asc');
		
		return $model;
	}

	public function inboxes()
	{
		$model = $this->iklan->select('iklans.id','messages.nama','messages.pesan','messages.email','messages.user_id','percakapan.from','percakapan.to','users.email as email_iklan','users.name as nama_iklan')
			->join('users','users.id','=','iklans.user_id')
			->leftJoin('messages','messages.iklan_id','=','iklans.id')
			->leftJoin('percakapan','messages.id','=','percakapan.message_id')
			->where('percakapan.to','=',auth()->user()->id)
			//->orWhere
			->orderBy('messages.created_at','desc');

		return $model;
	} 

	public function sentes()
	{
		$model = $this->iklan->select('iklans.id','messages.nama','messages.pesan','messages.email','messages.user_id','percakapan.from','percakapan.to','users.email as email_iklan','users.name as nama_iklan')
			->join('messages','messages.iklan_id','=','iklans.id')
			->join('users','users.id','=','iklans.user_id')
			->leftJoin('percakapan','messages.id','=','percakapan.message_id')
			->where('percakapan.from','=',auth()->user()->id)
			//->orWhere
			->orderBy('messages.created_at','desc');

		return $model;
	} 
}
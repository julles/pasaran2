<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Iklan;

class IklanView extends Model
{
    protected $table = 'iklan_views';

    public $guarded = [];

    public function iklan()
    {
    	return $this->belongsTo(Iklan::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Blog;

use App\Models\BlogCategory;

class BlogManyCategory extends Model
{
    protected $table = 'blog_many_category';

    public $guarded = [];

    public function blog()
    {
    	return $this->belongsTo(Blog::class,'blog_id');
    }

    public function category()
    {
    	return $this->belongsTo(BlogCategory::class,'category_id');
    }
}

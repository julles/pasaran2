<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Iklan;
use App\User;

class IklanLike extends Model
{
    protected $table = 'iklan_likes';

    public $guarded = [];

    public function iklan()
    {
    	return $this->belongsTo(Iklan::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}

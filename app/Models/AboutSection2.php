<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutSection2 extends Model
{
    protected $table = 'about_section2';

    public $guarded = [];
}

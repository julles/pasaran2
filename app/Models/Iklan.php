<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\User;
use App\Models\IklanImage;
use App\Models\IklanView;
use App\Models\Message;

class Iklan extends Model
{
    protected $table = 'iklans';

    public $guarded = [];

    public function rules()
    {
        return [
            'judul'         => 'required',
            'nama'          => 'required',
            'email'         => 'required|email|unique:users',
            'no_handphone'  => 'required',
            'agree'         => 'required',
        ];
    }

    public function messages()
    {
        return [
            'judul.required'            => 'judul tidak boleh kosong',
            'nama.required'             => 'nama tidak boleh kosong',
            'email.required'            => 'email tidak boleh kosong',
            'no_handphone.required'     => 'no handphone tidak boleh kosong',
            'agree.required'            => 'Syarat dan ketentuan harus di centang',
        ];
    }

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->hasMany(IklanImage::class);
    }

    public function views()
    {
        return $this->hasMany(IklanView::class);
    }

    public function likes()
    {
        return $this->hasMany(IklanLike::class,'iklan_id');
    }

    public function getMessages()
    {
        return $this->hasMany(Message::class,'iklan_id');
    }
}

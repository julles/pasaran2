<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\BlogCategory;

class Blog extends Model
{
    protected $table = 'blogs';

    public $guarded = [];

    public function rules()
    {
    	return [
    		'title'	=> 'required|max:335',
    		'image'	=> 'image|max:2000',
    	];
    }

    public function categories()
    {
    	return $this->belongsToMany(BlogCategory::class,'blog_many_category','blog_id','category_id');
    }
}

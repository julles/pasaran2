<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Iklan;
use App\User;
use App\Models\Percakapan;

class Message extends Model
{
    protected $table = 'messages';

    public $guarded = [];

    public function rules()
    {
        return [
            'nama'      => 'required',
            'email'     => 'required|email',
            'pesan'     => 'required',
        ];
    }

    public function user()
    {
		return $this->belongsTo(User::class);    			
    }

    public function iklan()
    {
		return $this->belongsTo(Iklan::class);		
    }

    public function percakapan()
    {
        return $this->hasOne(Percakapan::class,'id');
    }
}

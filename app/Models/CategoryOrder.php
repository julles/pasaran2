<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class CategoryOrder extends Model
{
    protected $table = 'category_orders';

    public $guarded = [];

    public function category()
    {
    	return $this->belongsTo(Category::class,'category_id');
    }
}

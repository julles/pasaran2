<?php

function webarq()
{
	return new App\Webarq\Src\Webarq;
}

function injectModel($model)
{
	$inject = "App\Models\\".$model;
	return new $inject;
}

function urlBackend($slug)
{
	return url(webarq()->backendUrl.'/'.$slug);
}

function urlBackendAction($action)
{
	return urlBackend(request()->segment(2).'/'.$action);
}

function getUser()
{
	return Auth::user();
}

function randomImage($str = "")
{
	return str_random(6).date("YmdHis");
}

function flash($title,$message,$type)
{
	return "<script>
		swal({
			type : '$type',
			html : true,
			title : '$title',
			text : '".$message."',
		});
	</script>";
}

function flashValidation($errors)
{
	$str="";
	foreach($errors as $error)
	{
		$str.= $error."<br/>";
	}

	return "<script>
		swal({
			type : 'error',
			html : true,
			title : 'Error Validation',
			text : '".$str."',
		});
	</script>";
}

function me()
{
	return auth()->user();
}

function carbon()
{
	return new \Carbon\Carbon;
}

function waktuLalu($date)
{
	$waktu = carbon()->now()->diffForHumans($date);

	return str_replace("setelah", "yang lalu", $waktu);
}

function money($money)
{
  /**
     * Convert number ke format Uang 
     * Penggunaan : formatMoney(1000)
     * output : 1.000
  */
      	if(is_numeric($money))
      	{
      		
	        $changeFormat = number_format($money);
		$replace = str_replace(",", ".", $changeFormat);
		$fixMoney = $replace;
        }else{
        
        	$fixMoney = $money.' not number';
      	
        }
        
        return $fixMoney;
}

function count_likes()
{
	$model = injectModel('IklanLike');
	if(\Auth::check())
	{
		$count = $model->whereUserId(auth()->user()->id)->count();
	}else{
		$count = $model->whereIp(request()->ip())->whereUserId(null)->count();
	}
	return $count;
}

function unlinkContents($imageName)
{
	$paths = [null,'thumbnails/'];

	foreach($paths as $path)
	{
		@unlink(public_path('contents/'.$path.$imageName));
	}
}